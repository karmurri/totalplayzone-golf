package sitwifi.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerificaUsuario {
    static Logger logger = LogManager.getLogger(VerificaUsuario.class.getName());
    
    public VerificaUsuario() {
        super();
    }
    
    public static boolean isNumCuenta(String cuenta ){
        boolean isCuenta= false;
        Pattern pat= Pattern.compile("^[\\d]{1}\\.[\\d]{7}");  
        Matcher mat= pat.matcher(cuenta);
        if(mat.matches()) isCuenta=true;
        return isCuenta;
    }
    
    public static boolean isNumTelefono(String telefono){
        boolean isTelefono= false;
        Pattern pat= Pattern.compile("^[\\d]{9,10}");
        Matcher mat= pat.matcher(telefono);
        if(mat.matches()) isTelefono=true;
        return isTelefono;
    }
    
    public static boolean isNumCuentaVarios(String cuenta ){
        boolean isCuenta= false;
        Pattern pat= Pattern.compile("^(([\\d]{1}\\.[\\d]{7})|((01|02)[\\d]{8}))");  
        Matcher mat= pat.matcher(cuenta);
        if(mat.matches()) isCuenta=true;
        return isCuenta;
    }
    
}
