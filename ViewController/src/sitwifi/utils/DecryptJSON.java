package sitwifi.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import sitwifi.utils.FileProperty;
import sitwifi.encrypt.AesUtil;

public final class DecryptJSON {
    final static Logger logger = LogManager.getLogger(DecryptJSON.class);
    public DecryptJSON() {
        super();
    }
    
    public static String[] DecryptJSONString(String cadJson){
        String[] result = new String[2];
        
        try{
            FileProperty fp = new FileProperty("sitwifi.prop.MTZWS");
            String iv = fp.getProperty("VI");
            String salt = fp.getProperty("TLAS");
            String passphrase = fp.getProperty("ESARHPSSAP");
            int ic = Integer.decode(fp.getProperty("IC"));
            int ks = Integer.decode(fp.getProperty("KS"));
            AesUtil au = new AesUtil(ks,ic);
            String json = au.decrypt(salt, iv, passphrase, cadJson);
            JSONObject jsonObject = new JSONObject();
            JSONParser parser = new JSONParser();
            jsonObject = (JSONObject)parser.parse(json);
            JSONObject data = (JSONObject)jsonObject.get(json);
            
            String data0 = (String)jsonObject.get("usr");
            String data1 = (String)jsonObject.get("pss");
            //String data2 = (String)jsonObject.get("mac");
           
            if(data0!=null && data1!=null){
                result[0] = data0;
                result[1] = data1;
                //result[2] = data2;
            }else{
                result[0] = null;
                result[1] = null;
                //result[2] = null;
                    
            }
        }catch(Exception e){
            logger.error("Error: "+e.getMessage()+" : "+e.getCause());
        }
        
        return result;
    }
    
    public static String[] DecryptJSONRecomendado(String cadJson){
        String[] result = new String[3];
        
        try{
            FileProperty fp = new FileProperty("com.totalplay.prop.cfg");
            String iv = fp.getProperty("VI");
            String salt = fp.getProperty("TLAS");
            String passphrase = fp.getProperty("ESARHPSSAP");
            int ic = Integer.decode(fp.getProperty("IC"));
            int ks = Integer.decode(fp.getProperty("KS"));
            
            AesUtil au = new AesUtil(ks,ic);
            
            String json = au.decrypt(salt, iv, passphrase, cadJson);
            JSONObject jsonObject = new JSONObject();
            JSONParser parser = new JSONParser();
            jsonObject = (JSONObject)parser.parse(json);
            
            String data0 = (String)jsonObject.get("nombre");
            String data1 = (String)jsonObject.get("tel");
            String data2 = (String)jsonObject.get("correo");
            
            if(data0!=null && data1!=null && data2!=null){
                result[0] = data0;
                result[1] = data1;
                result[2] = data2;
            }else{
                result[0] = null;
                result[1] = null;
                result[2] = null;
                    
            }
            
            
        }catch(Exception e){
            logger.error("Error: "+e.getMessage()+" : "+e.getCause());
        }
        
        return result;
    }
    
    public static String[] DecryptJSONParams(String cadJson){
        String[] result = new String[2];
        
        try{
            FileProperty fp = new FileProperty("sitwifi.prop.MTZWS");
            String iv = fp.getProperty("VI");
            String salt = fp.getProperty("TLAS");
            String passphrase = fp.getProperty("ESARHPSSAP");
            int ic = Integer.decode(fp.getProperty("IC"));
            int ks = Integer.decode(fp.getProperty("KS"));
            AesUtil au = new AesUtil(ks,ic);
            String json = au.decrypt(salt, iv, passphrase, cadJson);
            JSONObject jsonObject = new JSONObject();
            JSONParser parser = new JSONParser();
            jsonObject = (JSONObject)parser.parse(json);
            JSONObject data = (JSONObject)jsonObject.get(json);
            
            String data0 = (String)jsonObject.get("ip");
            String data1 = (String)jsonObject.get("mac");
           
            if(data0!=null && data1!=null){
                result[0] = data0;
                result[1] = data1;
            }else{
                result[0] = null;
                result[1] = null;
                    
            }
        }catch(Exception e){
            logger.error("Error: "+e.getMessage()+" : "+e.getCause());
        }
        
        return result;
    }
}
