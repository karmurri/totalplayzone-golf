package sitwifi.utils;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileProperty {
    Logger logger = LogManager.getLogger(this.getClass().getName());
	private  ResourceBundle bundle = null;
    public FileProperty(String absoluteRoute){
            try{
                bundle = ResourceBundle.getBundle(absoluteRoute, Locale.getDefault());    
            }catch(MissingResourceException e){
                logger.error("Error al carga el Archivo Properties",e);
            } 
    }
    /**
      * M�todo para la obtencion de la propiedad
      * @param String nombre de la propiedad
      * @return String valor de la propiedad
      */ 
 public String getProperty(String prop) {
    if(bundle!=null){
        String cad="";
        try{
          cad=bundle.getString(prop);
          return cad;
        }catch(Exception e){
               logger.error("Existe algun error con la propiedad", e);
               return cad;
           }
    }else{
         //logger.info("el objeto se encuentra en  null");
        return null;
     }  
    }
}
