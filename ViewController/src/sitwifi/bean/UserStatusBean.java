package sitwifi.bean;

public class UserStatusBean {
    private String nombre;
    private int status;
    private String noCuenta;

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setNoCuenta(String noCuenta) {
        this.noCuenta = noCuenta;
    }

    public String getNoCuenta() {
        return noCuenta;
    }

    public UserStatusBean() {
        super();
    }
}
