package sitwifi.implementServices;

import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.BPELGetInfoBRM;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.BpelgetinfobrmClientEp;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse;
import com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.ProcessResponse.DatosCliente;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sitwifi.bean.UserStatusBean;

import sitwifi.utils.Constantes;
import sitwifi.utils.FileProperty;

public class UserStatus {
    
    FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    Logger logger = LogManager.getLogger(this.getClass().getName());
    public UserStatus() {
        super();
    }
    
    
    public UserStatusBean getBean(String cta, boolean telefono){
        UserStatusBean usb= new UserStatusBean();
        
        BpelgetinfobrmClientEp bpelinfobrmClienteEp = null;
        try{
            URL url = new URL(fileProperty.getProperty("INFOBRM"));
            bpelinfobrmClienteEp = new BpelgetinfobrmClientEp();
        }catch(MalformedURLException e){
            logger.error("No se pudo inicializar el bpel infobrm con la url de InfoBrm");
        }
        if(bpelinfobrmClienteEp != null){
            BPELGetInfoBRM infoBrm = bpelinfobrmClienteEp.getBPELGetInfoBRMPt();
            com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.Process payload = new com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm.Process();
            if(!telefono)
                payload.setAccountNo(cta);
            else
                payload.setDnTotalplay(cta);
            ProcessResponse pr = infoBrm.process(payload);
            if(pr.getResponse().getCode().equals("0")){
                DatosCliente dc=pr.getDatosCliente().get(0);
                usb.setNombre(dc.getName());
                usb.setNoCuenta(dc.getAccountNo());
                try {
                    usb.setStatus(Integer.parseInt(dc.getStatusCuenta()));
                } catch (NumberFormatException nfe) {
                    usb.setStatus(0);
                }
                
            }
        }
        return usb;
    }
}
