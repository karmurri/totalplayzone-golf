package sitwifi.implementServices;


import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BPELgetInfoBRM;
import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BpelgetinfobrmClientEp;

import com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse;

import java.net.MalformedURLException;
import java.net.URL;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sitwifi.utils.Constantes;
import sitwifi.utils.FileProperty;


public class ValidateMorosoUtil {
    Logger logger = LogManager.getLogger(this.getClass().getName());
    FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
    
    public boolean isMoroso(String cuenta){
        boolean isMoroso=true;

        BpelgetinfobrmClientEp bpelgetinfobrmClientEp = null;
        BPELgetInfoBRM bPELgetInfoBRM = null;
        try {
            
            URL url = new URL(fileProperty.getProperty("CONSULTABRM"));
            bpelgetinfobrmClientEp =
                new BpelgetinfobrmClientEp(url,
                                            new QName("http://xmlns.oracle.com/MiddlewareApplication/BrmSearchAccount/BPELgetInfoBRM",
                                            "bpelgetinfobrm_client_ep"));

        } catch (MalformedURLException murle) {
            logger.error("No se pudo conectar con SOA");
            return true;
        }    
        
        if(bpelgetinfobrmClientEp!=null){
            bPELgetInfoBRM=bpelgetinfobrmClientEp.getBPELgetInfoBRMPt();
            
            if(bPELgetInfoBRM!=null){
                Holder<String> idResult= new Holder<String>();
                Holder<String> result= new Holder<String>();
                Holder<String> resultDescription= new Holder<String>();
                Holder<com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM> arregloBRM=new Holder<com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ProcessResponse.ArrBRM>();
                bPELgetInfoBRM.process(cuenta, idResult, result, resultDescription, arregloBRM);
                
                if(result!=null && result.value.equals("0")){
                    ProcessResponse.ArrBRM.BillInfo.LastBillBalance resumen;
                    resumen=arregloBRM.value.getBillInfo().getLastBillBalance();
                    
                    if(resumen!=null){
                        if(resumen.getCurrentBilling().doubleValue()<=1.0){
                            return false;
                        }else{
                            if(!resumen.getEndT().equals("") && !resumen.getEndTDiscount().equals("")){
                                Date prontoPago= convierteFechaSimple(resumen.getEndTDiscount());
                                Date vencimiento= convierteFechaSimple(resumen.getEndT());
                                if(obtieneFechaActual("dd/MM/yyyy").before(prontoPago)){
                                    return false;
                                }
                                else if(obtieneFechaActual("dd/MM/yyyy").after(vencimiento)){
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            }
                        }
                        
                    }else{
                        logger.error("No se pueden consultar los datos BRMSearchAccount");
                        return true;
                    }
                    
                    
                }else{
                    logger.error("No se pueden consultar los datos BRMSearchAccount");
                    return true;
                }
            }else{
                logger.error("No se pudo conectar con el ENDPOINT BRMSearchAccount");
                return true;    
            }
        }else{
            return true;
        }
        
        
        
        
        
        
        return isMoroso;
    }
    public Date convierteFechaSimple(String fecha){
        SimpleDateFormat sdf=null;
        if(fecha.contains("/")){
            sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("es","MX"));
        }else if(fecha.contains("-")){
            sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es","MX"));
        }
        Date result=null;
        if(sdf!=null){
            try {
            result=sdf.parse(fecha);
            } catch (ParseException e) {
                logger.error("Error en conversión de fecha: "+e.getMessage());
            }
        }
       return (result);
    }
    
    public Date obtieneFechaActual(String formato){
        SimpleDateFormat sdf= new SimpleDateFormat(formato, new Locale("es","MX"));
        Date fecha= new Date();
        String fechaF=sdf.format(fecha);
        Date result=null;
        try {
            result =sdf.parse(fechaF);
        } catch (ParseException e) {
             logger.error("Error en conversión de fecha: "+e.getMessage());
        }
        return result;
    }
}
