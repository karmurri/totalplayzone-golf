package sitwifi.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import sitwifi.implementServices.ValidateMorosoUtil;

import sitwifi.utils.FileProperty;
import sitwifi.utils.Constantes;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;
import org.json.JSONException;

import org.tempuri.SecurityUserModel;
import org.tempuri.Service1;

import org.tempuri.Service1Soap;

import sitwifi.bean.UserStatusBean;

import sitwifi.implementServices.UserStatus;

import sitwifi.utils.*;


@WebServlet(name = "ServletLogin", urlPatterns = { "/servletlogin" })
public class ServletLogin extends HttpServlet {
    @SuppressWarnings("compatibility:-1764498662527631192")
    private static final long serialVersionUID = 1L;
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    @SuppressWarnings("oracle.jdeveloper.java.field-not-serializable")
    Logger logger = LogManager.getLogger(this.getClass().getName());

    /**
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        
        FileProperty fileProperty = new FileProperty(Constantes.WSROUTEPROP);
        SecurityUserModel infoUser = null;
        
        String mac = null;
        String ip = null;
        String error = null;
        
        HttpSession sesion = request.getSession(false);
        mac = (String)sesion.getAttribute("mac");
        ip = (String)sesion.getAttribute("ip");
        logger.error("ip login: "+ip+", mac login: "+mac);
        String encriptado = request.getParameter("data");
        String[] datosDes = new String[2];
        if(encriptado != null){
            datosDes = DecryptJSON.DecryptJSONString(encriptado);
        }

        String cta = datosDes[0];
        String pss = datosDes[1];

        if(VerificaUsuario.isNumCuentaVarios(cta)){            
            Service1 service1 = null;
            try{
                URL url = new URL(fileProperty.getProperty("ENGINESECURITY"));
                service1 = new Service1(url,new QName("http://tempuri.org/", "Service1"));
            }catch(MalformedURLException e){
                logger.error("No se pudo inicializar el bpel service1 con la url proporcionada "+e.getMessage());
            }
            if(service1 != null){
                Service1Soap service1Soap = service1.getService1Soap12();
                try{
                    infoUser = service1Soap.loginUser(cta, pss);
                    if(infoUser != null){
                        UserStatus us=new UserStatus();
                        UserStatusBean usb=us.getBean(cta, false);
                        
                        if(usb.getStatus()==10100){
                            if(!new ValidateMorosoUtil().isMoroso(cta)){
                                error = "exito";
                            }else{
                                error = "moroso";
                            }
                        }else{
                            logger.error("El usuario esta cancelado o inactivo");
                            error = "cancelado";
                        }
                    }else{
                        logger.error("infoUser es nulo");
                        error = "credenciales";
                    }
                }catch(Exception ex){
                    logger.error("Error al hacer login");
                    ex.printStackTrace();
                    infoUser = null;
                    error = "error";
                }
            }else{
                logger.error("service1 es nulo");
                error = "error";
            }
        }else if(VerificaUsuario.isNumTelefono(cta)){
            logger.error("es numero de telefono");
            UserStatus us=new UserStatus();
            UserStatusBean usb=us.getBean(cta, true);
            if(usb.getStatus()==10100){
                    String numCta = usb.getNoCuenta();
                    Service1 service1 = null;
                    try{
                        URL url = new URL(fileProperty.getProperty("ENGINESECURITY"));
                        service1 = new Service1(url,new QName("http://tempuri.org/", "Service1"));
                    }catch(MalformedURLException e){
                        logger.error("No se pudo inicializar el bpel service1 con la url proporcionada "+e.getMessage());
                    }
                    if(service1 != null){
                        Service1Soap service1Soap = service1.getService1Soap12();
                        try{
                            infoUser = service1Soap.loginUser(numCta, pss);
                            if(infoUser != null){
                                if(!new ValidateMorosoUtil().isMoroso(numCta)){
                                    error = "exito";
                                }else{
                                    error = "moroso";
                                }
                            }else{
                                error = "credenciales";
                            }
                        }catch(Exception e){
                            logger.error("Error al hacer login "
                                         +e.getMessage());
                            infoUser = null;
                            error = "error";
                        }
                    }else{
                        logger.error("service1 nulo");
                        error = "error";
                    }
            }else{
                logger.error("DN erroneo");
                error = "credenciales";
            }
            
        }

        JSONObject obj = new JSONObject();
        try{
            if(ip != null && mac != null){
                obj.put("ip",ip);
                obj.put("mac",mac);
            }
            if(error == null){
                error = "error";
            }
            obj.put("error",error);
        }catch(JSONException ex){
            logger.error("No se pudo formar el json "+ex.getMessage());
        }
        
        out.print(obj);
        out.close();
    }
}
