package sitwifi.servlets;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.json.JSONException;
import org.json.JSONObject;
import sitwifi.utils.*;

@WebServlet(name = "ServletIndex", urlPatterns = { "/servletindex" })
public class ServletIndex extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    Logger logger = LogManager.getLogger(this.getClass().getName());

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        
        HttpSession sesion= request.getSession(false);
        
        String error = null;
        String ip = null;
        String mac = null;
        
        String encriptado = request.getParameter("data");
        String[] datosDes = new String[2];
        if(encriptado != null){
            datosDes = DecryptJSON.DecryptJSONParams(encriptado);
        }
        
        ip = datosDes[0];
        mac = datosDes[1];
        logger.error("ip index: "+ip+", mac index: "+mac);
        if((ip.equalsIgnoreCase("") || ip != null) && (mac.equalsIgnoreCase("") || mac != null )){
            sesion.setAttribute("ip", ip);
            sesion.setAttribute("mac", mac); 
            error = "exito";
        }
        
        JSONObject obj = new JSONObject();
        try{
            if(error == null){
                error = "error";
            }
            obj.put("error",error);
        }catch(JSONException ex){
            logger.error("No se pudo formar el json "+ex.getMessage());
        }
        
        out.print(obj);
        out.close();
    }
}
