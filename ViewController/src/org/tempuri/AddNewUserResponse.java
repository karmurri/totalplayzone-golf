
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddNewUserResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "addNewUserResult" })
@XmlRootElement(name = "AddNewUserResponse")
public class AddNewUserResponse {

    @XmlElement(name = "AddNewUserResult")
    protected int addNewUserResult;

    /**
     * Gets the value of the addNewUserResult property.
     *
     */
    public int getAddNewUserResult() {
        return addNewUserResult;
    }

    /**
     * Sets the value of the addNewUserResult property.
     *
     */
    public void setAddNewUserResult(int value) {
        this.addNewUserResult = value;
    }

}
