
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoginUserByEmailResult" type="{http://tempuri.org/}SecurityUserModel" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "loginUserByEmailResult" })
@XmlRootElement(name = "LoginUserByEmailResponse")
public class LoginUserByEmailResponse {

    @XmlElement(name = "LoginUserByEmailResult")
    protected SecurityUserModel loginUserByEmailResult;

    /**
     * Gets the value of the loginUserByEmailResult property.
     *
     * @return
     *     possible object is
     *     {@link SecurityUserModel }
     *
     */
    public SecurityUserModel getLoginUserByEmailResult() {
        return loginUserByEmailResult;
    }

    /**
     * Sets the value of the loginUserByEmailResult property.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityUserModel }
     *
     */
    public void setLoginUserByEmailResult(SecurityUserModel value) {
        this.loginUserByEmailResult = value;
    }

}
