
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserModel" type="{http://tempuri.org/}SecurityUserModel" minOccurs="0"/&gt;
 *         &lt;element name="UserRolModel" type="{http://tempuri.org/}SecurityUserOnRolModel" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userModel", "userRolModel" })
@XmlRootElement(name = "AddNewUser")
public class AddNewUser {

    @XmlElement(name = "UserModel")
    protected SecurityUserModel userModel;
    @XmlElement(name = "UserRolModel")
    protected SecurityUserOnRolModel userRolModel;

    /**
     * Gets the value of the userModel property.
     *
     * @return
     *     possible object is
     *     {@link SecurityUserModel }
     *
     */
    public SecurityUserModel getUserModel() {
        return userModel;
    }

    /**
     * Sets the value of the userModel property.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityUserModel }
     *
     */
    public void setUserModel(SecurityUserModel value) {
        this.userModel = value;
    }

    /**
     * Gets the value of the userRolModel property.
     *
     * @return
     *     possible object is
     *     {@link SecurityUserOnRolModel }
     *
     */
    public SecurityUserOnRolModel getUserRolModel() {
        return userRolModel;
    }

    /**
     * Sets the value of the userRolModel property.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityUserOnRolModel }
     *
     */
    public void setUserRolModel(SecurityUserOnRolModel value) {
        this.userRolModel = value;
    }

}
