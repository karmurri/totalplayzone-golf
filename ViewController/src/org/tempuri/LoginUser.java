
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="strUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="strPWD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "strUser", "strPWD" })
@XmlRootElement(name = "LoginUser")
public class LoginUser {

    protected String strUser;
    protected String strPWD;

    /**
     * Gets the value of the strUser property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStrUser() {
        return strUser;
    }

    /**
     * Sets the value of the strUser property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStrUser(String value) {
        this.strUser = value;
    }

    /**
     * Gets the value of the strPWD property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStrPWD() {
        return strPWD;
    }

    /**
     * Sets the value of the strPWD property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStrPWD(String value) {
        this.strPWD = value;
    }

}
