
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetUserByEmployeeNumberResult" type="{http://tempuri.org/}SecurityUserModel" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getUserByEmployeeNumberResult" })
@XmlRootElement(name = "GetUserByEmployeeNumberResponse")
public class GetUserByEmployeeNumberResponse {

    @XmlElement(name = "GetUserByEmployeeNumberResult")
    protected SecurityUserModel getUserByEmployeeNumberResult;

    /**
     * Gets the value of the getUserByEmployeeNumberResult property.
     *
     * @return
     *     possible object is
     *     {@link SecurityUserModel }
     *
     */
    public SecurityUserModel getGetUserByEmployeeNumberResult() {
        return getUserByEmployeeNumberResult;
    }

    /**
     * Sets the value of the getUserByEmployeeNumberResult property.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityUserModel }
     *
     */
    public void setGetUserByEmployeeNumberResult(SecurityUserModel value) {
        this.getUserByEmployeeNumberResult = value;
    }

}
