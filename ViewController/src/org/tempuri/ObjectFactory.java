
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.tempuri package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddNewUser }
     *
     */
    public AddNewUser createAddNewUser() {
        return new AddNewUser();
    }

    /**
     * Create an instance of {@link SecurityUserModel }
     *
     */
    public SecurityUserModel createSecurityUserModel() {
        return new SecurityUserModel();
    }

    /**
     * Create an instance of {@link SecurityUserOnRolModel }
     *
     */
    public SecurityUserOnRolModel createSecurityUserOnRolModel() {
        return new SecurityUserOnRolModel();
    }

    /**
     * Create an instance of {@link AddNewUserResponse }
     *
     */
    public AddNewUserResponse createAddNewUserResponse() {
        return new AddNewUserResponse();
    }

    /**
     * Create an instance of {@link LoginUser }
     *
     */
    public LoginUser createLoginUser() {
        return new LoginUser();
    }

    /**
     * Create an instance of {@link LoginUserResponse }
     *
     */
    public LoginUserResponse createLoginUserResponse() {
        return new LoginUserResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     *
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     *
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link CloseSession }
     *
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     *
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link UpdateUser }
     *
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     *
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link GetUserByEmployeeNumber }
     *
     */
    public GetUserByEmployeeNumber createGetUserByEmployeeNumber() {
        return new GetUserByEmployeeNumber();
    }

    /**
     * Create an instance of {@link GetUserByEmployeeNumberResponse }
     *
     */
    public GetUserByEmployeeNumberResponse createGetUserByEmployeeNumberResponse() {
        return new GetUserByEmployeeNumberResponse();
    }

    /**
     * Create an instance of {@link LoginUserByEmail }
     *
     */
    public LoginUserByEmail createLoginUserByEmail() {
        return new LoginUserByEmail();
    }

    /**
     * Create an instance of {@link LoginUserByEmailResponse }
     *
     */
    public LoginUserByEmailResponse createLoginUserByEmailResponse() {
        return new LoginUserByEmailResponse();
    }

}
