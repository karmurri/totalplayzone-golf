
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RESULTS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="AAC_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_PACKAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_PROMO_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_SERIAL_NUM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_SOURCE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_VENDOR" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ACCESS_CODE1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ACCESS_CODE2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ACCOUNT_TAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ACCOUNT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ATTRIBUTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BAL_GRP_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BRAND_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BUSINESS_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CLOSE_WHEN_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CREATED_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CURRENCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CURRENCY_SECONDARY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CUSTOMER_SEGMENT_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="EFFECTIVE_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="GL_SEGMENT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="GROUP_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INCORPORATED_FLAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INTERNAL_NOTES" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ITEM_POID_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LASTSTAT_CMNT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LAST_STATUS_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LINEAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LOCALE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MOD_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="NAMEINFO" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="NEXT_ITEM_POID_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="OBJECT_CACHE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="READ_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RESIDENCE_FLAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="STATUS_FLAGS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TIMEZONE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="VAT_CERT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="WRITE_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "poid", "results" })
@XmlRootElement(name = "AuxOutputAccount")
public class AuxOutputAccount {

    @XmlElement(name = "POID", required = true)
    protected String poid;
    @XmlElement(name = "RESULTS", required = true)
    protected AuxOutputAccount.RESULTS results;

    /**
     * Gets the value of the poid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPOID() {
        return poid;
    }

    /**
     * Sets the value of the poid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPOID(String value) {
        this.poid = value;
    }

    /**
     * Gets the value of the results property.
     *
     * @return
     *     possible object is
     *     {@link AuxOutputAccount.RESULTS }
     *
     */
    public AuxOutputAccount.RESULTS getRESULTS() {
        return results;
    }

    /**
     * Sets the value of the results property.
     *
     * @param value
     *     allowed object is
     *     {@link AuxOutputAccount.RESULTS }
     *
     */
    public void setRESULTS(AuxOutputAccount.RESULTS value) {
        this.results = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AAC_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_PACKAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_PROMO_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_SERIAL_NUM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_SOURCE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_VENDOR" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ACCESS_CODE1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ACCESS_CODE2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ACCOUNT_TAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ACCOUNT_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ATTRIBUTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BAL_GRP_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BRAND_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BUSINESS_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CLOSE_WHEN_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CREATED_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CURRENCY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CURRENCY_SECONDARY" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CUSTOMER_SEGMENT_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="EFFECTIVE_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="GL_SEGMENT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="GROUP_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INCORPORATED_FLAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INTERNAL_NOTES" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ITEM_POID_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LASTSTAT_CMNT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LAST_STATUS_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LINEAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LOCALE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MOD_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="NAMEINFO" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="NEXT_ITEM_POID_LIST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="OBJECT_CACHE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="READ_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RESIDENCE_FLAG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="STATUS_FLAGS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TIMEZONE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="VAT_CERT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="WRITE_ACCESS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder =
             { "aacaccess", "aacpackage", "aacpromocode", "aacserialnum", "aacsource", "aacvendor", "accesscode1",
               "accesscode2", "accountno", "accounttag", "accounttype", "attribute", "balgrpobj", "brandobj",
               "businesstype", "closewhent", "createdt", "currency", "currencysecondary", "customersegmentlist",
               "effectivet", "glsegment", "groupobj", "incorporatedflag", "internalnotes", "itempoidlist",
               "laststatcmnt", "laststatust", "lineage", "locale", "modt", "name", "nameinfo", "nextitempoidlist",
               "objectcachetype", "poid", "readaccess", "residenceflag", "status", "statusflags", "timezoneid",
               "vatcert", "writeaccess"
        })
    public static class RESULTS {

        @XmlElement(name = "AAC_ACCESS", required = true)
        protected String aacaccess;
        @XmlElement(name = "AAC_PACKAGE", required = true)
        protected String aacpackage;
        @XmlElement(name = "AAC_PROMO_CODE", required = true)
        protected String aacpromocode;
        @XmlElement(name = "AAC_SERIAL_NUM", required = true)
        protected String aacserialnum;
        @XmlElement(name = "AAC_SOURCE", required = true)
        protected String aacsource;
        @XmlElement(name = "AAC_VENDOR", required = true)
        protected String aacvendor;
        @XmlElement(name = "ACCESS_CODE1", required = true)
        protected String accesscode1;
        @XmlElement(name = "ACCESS_CODE2", required = true)
        protected String accesscode2;
        @XmlElement(name = "ACCOUNT_NO", required = true)
        protected String accountno;
        @XmlElement(name = "ACCOUNT_TAG", required = true)
        protected String accounttag;
        @XmlElement(name = "ACCOUNT_TYPE", required = true)
        protected String accounttype;
        @XmlElement(name = "ATTRIBUTE", required = true)
        protected String attribute;
        @XmlElement(name = "BAL_GRP_OBJ", required = true)
        protected String balgrpobj;
        @XmlElement(name = "BRAND_OBJ", required = true)
        protected String brandobj;
        @XmlElement(name = "BUSINESS_TYPE", required = true)
        protected String businesstype;
        @XmlElement(name = "CLOSE_WHEN_T", required = true)
        protected String closewhent;
        @XmlElement(name = "CREATED_T", required = true)
        protected String createdt;
        @XmlElement(name = "CURRENCY", required = true)
        protected String currency;
        @XmlElement(name = "CURRENCY_SECONDARY", required = true)
        protected String currencysecondary;
        @XmlElement(name = "CUSTOMER_SEGMENT_LIST", required = true)
        protected String customersegmentlist;
        @XmlElement(name = "EFFECTIVE_T", required = true)
        protected String effectivet;
        @XmlElement(name = "GL_SEGMENT", required = true)
        protected String glsegment;
        @XmlElement(name = "GROUP_OBJ", required = true)
        protected String groupobj;
        @XmlElement(name = "INCORPORATED_FLAG", required = true)
        protected String incorporatedflag;
        @XmlElement(name = "INTERNAL_NOTES", required = true)
        protected String internalnotes;
        @XmlElement(name = "ITEM_POID_LIST", required = true)
        protected String itempoidlist;
        @XmlElement(name = "LASTSTAT_CMNT", required = true)
        protected String laststatcmnt;
        @XmlElement(name = "LAST_STATUS_T", required = true)
        protected String laststatust;
        @XmlElement(name = "LINEAGE", required = true)
        protected String lineage;
        @XmlElement(name = "LOCALE", required = true)
        protected String locale;
        @XmlElement(name = "MOD_T", required = true)
        protected String modt;
        @XmlElement(name = "NAME", required = true)
        protected String name;
        @XmlElement(name = "NAMEINFO", required = true)
        protected List<AuxOutputAccount.RESULTS.NAMEINFO> nameinfo;
        @XmlElement(name = "NEXT_ITEM_POID_LIST", required = true)
        protected String nextitempoidlist;
        @XmlElement(name = "OBJECT_CACHE_TYPE", required = true)
        protected String objectcachetype;
        @XmlElement(name = "POID", required = true)
        protected String poid;
        @XmlElement(name = "READ_ACCESS", required = true)
        protected String readaccess;
        @XmlElement(name = "RESIDENCE_FLAG", required = true)
        protected String residenceflag;
        @XmlElement(name = "STATUS", required = true)
        protected String status;
        @XmlElement(name = "STATUS_FLAGS", required = true)
        protected String statusflags;
        @XmlElement(name = "TIMEZONE_ID", required = true)
        protected String timezoneid;
        @XmlElement(name = "VAT_CERT", required = true)
        protected String vatcert;
        @XmlElement(name = "WRITE_ACCESS", required = true)
        protected String writeaccess;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the aacaccess property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACACCESS() {
            return aacaccess;
        }

        /**
         * Sets the value of the aacaccess property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACACCESS(String value) {
            this.aacaccess = value;
        }

        /**
         * Gets the value of the aacpackage property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACPACKAGE() {
            return aacpackage;
        }

        /**
         * Sets the value of the aacpackage property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACPACKAGE(String value) {
            this.aacpackage = value;
        }

        /**
         * Gets the value of the aacpromocode property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACPROMOCODE() {
            return aacpromocode;
        }

        /**
         * Sets the value of the aacpromocode property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACPROMOCODE(String value) {
            this.aacpromocode = value;
        }

        /**
         * Gets the value of the aacserialnum property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACSERIALNUM() {
            return aacserialnum;
        }

        /**
         * Sets the value of the aacserialnum property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACSERIALNUM(String value) {
            this.aacserialnum = value;
        }

        /**
         * Gets the value of the aacsource property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACSOURCE() {
            return aacsource;
        }

        /**
         * Sets the value of the aacsource property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACSOURCE(String value) {
            this.aacsource = value;
        }

        /**
         * Gets the value of the aacvendor property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACVENDOR() {
            return aacvendor;
        }

        /**
         * Sets the value of the aacvendor property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACVENDOR(String value) {
            this.aacvendor = value;
        }

        /**
         * Gets the value of the accesscode1 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCESSCODE1() {
            return accesscode1;
        }

        /**
         * Sets the value of the accesscode1 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCESSCODE1(String value) {
            this.accesscode1 = value;
        }

        /**
         * Gets the value of the accesscode2 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCESSCODE2() {
            return accesscode2;
        }

        /**
         * Sets the value of the accesscode2 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCESSCODE2(String value) {
            this.accesscode2 = value;
        }

        /**
         * Gets the value of the accountno property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTNO() {
            return accountno;
        }

        /**
         * Sets the value of the accountno property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTNO(String value) {
            this.accountno = value;
        }

        /**
         * Gets the value of the accounttag property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTTAG() {
            return accounttag;
        }

        /**
         * Sets the value of the accounttag property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTTAG(String value) {
            this.accounttag = value;
        }

        /**
         * Gets the value of the accounttype property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTTYPE() {
            return accounttype;
        }

        /**
         * Sets the value of the accounttype property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTTYPE(String value) {
            this.accounttype = value;
        }

        /**
         * Gets the value of the attribute property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getATTRIBUTE() {
            return attribute;
        }

        /**
         * Sets the value of the attribute property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setATTRIBUTE(String value) {
            this.attribute = value;
        }

        /**
         * Gets the value of the balgrpobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBALGRPOBJ() {
            return balgrpobj;
        }

        /**
         * Sets the value of the balgrpobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBALGRPOBJ(String value) {
            this.balgrpobj = value;
        }

        /**
         * Gets the value of the brandobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBRANDOBJ() {
            return brandobj;
        }

        /**
         * Sets the value of the brandobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBRANDOBJ(String value) {
            this.brandobj = value;
        }

        /**
         * Gets the value of the businesstype property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBUSINESSTYPE() {
            return businesstype;
        }

        /**
         * Sets the value of the businesstype property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBUSINESSTYPE(String value) {
            this.businesstype = value;
        }

        /**
         * Gets the value of the closewhent property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCLOSEWHENT() {
            return closewhent;
        }

        /**
         * Sets the value of the closewhent property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCLOSEWHENT(String value) {
            this.closewhent = value;
        }

        /**
         * Gets the value of the createdt property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCREATEDT() {
            return createdt;
        }

        /**
         * Sets the value of the createdt property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCREATEDT(String value) {
            this.createdt = value;
        }

        /**
         * Gets the value of the currency property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCURRENCY() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCURRENCY(String value) {
            this.currency = value;
        }

        /**
         * Gets the value of the currencysecondary property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCURRENCYSECONDARY() {
            return currencysecondary;
        }

        /**
         * Sets the value of the currencysecondary property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCURRENCYSECONDARY(String value) {
            this.currencysecondary = value;
        }

        /**
         * Gets the value of the customersegmentlist property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCUSTOMERSEGMENTLIST() {
            return customersegmentlist;
        }

        /**
         * Sets the value of the customersegmentlist property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCUSTOMERSEGMENTLIST(String value) {
            this.customersegmentlist = value;
        }

        /**
         * Gets the value of the effectivet property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getEFFECTIVET() {
            return effectivet;
        }

        /**
         * Sets the value of the effectivet property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setEFFECTIVET(String value) {
            this.effectivet = value;
        }

        /**
         * Gets the value of the glsegment property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getGLSEGMENT() {
            return glsegment;
        }

        /**
         * Sets the value of the glsegment property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setGLSEGMENT(String value) {
            this.glsegment = value;
        }

        /**
         * Gets the value of the groupobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getGROUPOBJ() {
            return groupobj;
        }

        /**
         * Sets the value of the groupobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setGROUPOBJ(String value) {
            this.groupobj = value;
        }

        /**
         * Gets the value of the incorporatedflag property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getINCORPORATEDFLAG() {
            return incorporatedflag;
        }

        /**
         * Sets the value of the incorporatedflag property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setINCORPORATEDFLAG(String value) {
            this.incorporatedflag = value;
        }

        /**
         * Gets the value of the internalnotes property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getINTERNALNOTES() {
            return internalnotes;
        }

        /**
         * Sets the value of the internalnotes property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setINTERNALNOTES(String value) {
            this.internalnotes = value;
        }

        /**
         * Gets the value of the itempoidlist property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getITEMPOIDLIST() {
            return itempoidlist;
        }

        /**
         * Sets the value of the itempoidlist property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setITEMPOIDLIST(String value) {
            this.itempoidlist = value;
        }

        /**
         * Gets the value of the laststatcmnt property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLASTSTATCMNT() {
            return laststatcmnt;
        }

        /**
         * Sets the value of the laststatcmnt property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLASTSTATCMNT(String value) {
            this.laststatcmnt = value;
        }

        /**
         * Gets the value of the laststatust property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLASTSTATUST() {
            return laststatust;
        }

        /**
         * Sets the value of the laststatust property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLASTSTATUST(String value) {
            this.laststatust = value;
        }

        /**
         * Gets the value of the lineage property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLINEAGE() {
            return lineage;
        }

        /**
         * Sets the value of the lineage property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLINEAGE(String value) {
            this.lineage = value;
        }

        /**
         * Gets the value of the locale property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLOCALE() {
            return locale;
        }

        /**
         * Sets the value of the locale property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLOCALE(String value) {
            this.locale = value;
        }

        /**
         * Gets the value of the modt property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getMODT() {
            return modt;
        }

        /**
         * Sets the value of the modt property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setMODT(String value) {
            this.modt = value;
        }

        /**
         * Gets the value of the name property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getNAME() {
            return name;
        }

        /**
         * Sets the value of the name property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setNAME(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the nameinfo property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nameinfo property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNAMEINFO().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AuxOutputAccount.RESULTS.NAMEINFO }
         *
         *
         */
        public List<AuxOutputAccount.RESULTS.NAMEINFO> getNAMEINFO() {
            if (nameinfo == null) {
                nameinfo = new ArrayList<AuxOutputAccount.RESULTS.NAMEINFO>();
            }
            return this.nameinfo;
        }

        /**
         * Gets the value of the nextitempoidlist property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getNEXTITEMPOIDLIST() {
            return nextitempoidlist;
        }

        /**
         * Sets the value of the nextitempoidlist property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setNEXTITEMPOIDLIST(String value) {
            this.nextitempoidlist = value;
        }

        /**
         * Gets the value of the objectcachetype property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOBJECTCACHETYPE() {
            return objectcachetype;
        }

        /**
         * Sets the value of the objectcachetype property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOBJECTCACHETYPE(String value) {
            this.objectcachetype = value;
        }

        /**
         * Gets the value of the poid property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPOID() {
            return poid;
        }

        /**
         * Sets the value of the poid property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPOID(String value) {
            this.poid = value;
        }

        /**
         * Gets the value of the readaccess property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getREADACCESS() {
            return readaccess;
        }

        /**
         * Sets the value of the readaccess property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setREADACCESS(String value) {
            this.readaccess = value;
        }

        /**
         * Gets the value of the residenceflag property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRESIDENCEFLAG() {
            return residenceflag;
        }

        /**
         * Sets the value of the residenceflag property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRESIDENCEFLAG(String value) {
            this.residenceflag = value;
        }

        /**
         * Gets the value of the status property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSTATUS() {
            return status;
        }

        /**
         * Sets the value of the status property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSTATUS(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the statusflags property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSTATUSFLAGS() {
            return statusflags;
        }

        /**
         * Sets the value of the statusflags property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSTATUSFLAGS(String value) {
            this.statusflags = value;
        }

        /**
         * Gets the value of the timezoneid property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTIMEZONEID() {
            return timezoneid;
        }

        /**
         * Sets the value of the timezoneid property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTIMEZONEID(String value) {
            this.timezoneid = value;
        }

        /**
         * Gets the value of the vatcert property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVATCERT() {
            return vatcert;
        }

        /**
         * Sets the value of the vatcert property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVATCERT(String value) {
            this.vatcert = value;
        }

        /**
         * Gets the value of the writeaccess property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getWRITEACCESS() {
            return writeaccess;
        }

        /**
         * Sets the value of the writeaccess property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setWRITEACCESS(String value) {
            this.writeaccess = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "",
                 propOrder =
                 { "description", "street", "numExterior", "numInterior", "city", "colonia", "delegacion",
                   "entreCalles", "latitud", "longitud", "country", "email", "state", "zip", "title", "firstName",
                   "middleName", "lastName", "birthday", "razonSocial", "rfc", "genero"
            })
        public static class NAMEINFO {

            @XmlElement(name = "Description", required = true)
            protected String description;
            @XmlElement(name = "Street", required = true)
            protected String street;
            @XmlElement(name = "NumExterior", required = true)
            protected String numExterior;
            @XmlElement(name = "NumInterior", required = true)
            protected String numInterior;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "Colonia", required = true)
            protected String colonia;
            @XmlElement(name = "Delegacion", required = true)
            protected String delegacion;
            @XmlElement(name = "EntreCalles", required = true)
            protected String entreCalles;
            @XmlElement(name = "Latitud", required = true)
            protected String latitud;
            @XmlElement(name = "Longitud", required = true)
            protected String longitud;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlElement(required = true)
            protected String email;
            @XmlElement(name = "State", required = true)
            protected String state;
            @XmlElement(name = "Zip", required = true)
            protected String zip;
            @XmlElement(name = "Title", required = true)
            protected String title;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "MiddleName", required = true)
            protected String middleName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "Birthday", required = true)
            protected String birthday;
            @XmlElement(name = "RazonSocial", required = true)
            protected String razonSocial;
            @XmlElement(name = "RFC", required = true)
            protected String rfc;
            @XmlElement(name = "Genero", required = true)
            protected String genero;
            @XmlAttribute(name = "elem")
            protected Integer elem;

            /**
             * Gets the value of the description property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the street property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStreet() {
                return street;
            }

            /**
             * Sets the value of the street property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Gets the value of the numExterior property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getNumExterior() {
                return numExterior;
            }

            /**
             * Sets the value of the numExterior property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setNumExterior(String value) {
                this.numExterior = value;
            }

            /**
             * Gets the value of the numInterior property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getNumInterior() {
                return numInterior;
            }

            /**
             * Sets the value of the numInterior property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setNumInterior(String value) {
                this.numInterior = value;
            }

            /**
             * Gets the value of the city property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the colonia property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getColonia() {
                return colonia;
            }

            /**
             * Sets the value of the colonia property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setColonia(String value) {
                this.colonia = value;
            }

            /**
             * Gets the value of the delegacion property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getDelegacion() {
                return delegacion;
            }

            /**
             * Sets the value of the delegacion property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setDelegacion(String value) {
                this.delegacion = value;
            }

            /**
             * Gets the value of the entreCalles property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getEntreCalles() {
                return entreCalles;
            }

            /**
             * Sets the value of the entreCalles property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setEntreCalles(String value) {
                this.entreCalles = value;
            }

            /**
             * Gets the value of the latitud property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getLatitud() {
                return latitud;
            }

            /**
             * Sets the value of the latitud property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setLatitud(String value) {
                this.latitud = value;
            }

            /**
             * Gets the value of the longitud property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getLongitud() {
                return longitud;
            }

            /**
             * Sets the value of the longitud property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setLongitud(String value) {
                this.longitud = value;
            }

            /**
             * Gets the value of the country property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the email property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets the value of the email property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setEmail(String value) {
                this.email = value;
            }

            /**
             * Gets the value of the state property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setZip(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the title property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTitle() {
                return title;
            }

            /**
             * Sets the value of the title property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTitle(String value) {
                this.title = value;
            }

            /**
             * Gets the value of the firstName property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the middleName property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getMiddleName() {
                return middleName;
            }

            /**
             * Sets the value of the middleName property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setMiddleName(String value) {
                this.middleName = value;
            }

            /**
             * Gets the value of the lastName property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the birthday property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBirthday() {
                return birthday;
            }

            /**
             * Sets the value of the birthday property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBirthday(String value) {
                this.birthday = value;
            }

            /**
             * Gets the value of the razonSocial property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getRazonSocial() {
                return razonSocial;
            }

            /**
             * Sets the value of the razonSocial property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setRazonSocial(String value) {
                this.razonSocial = value;
            }

            /**
             * Gets the value of the rfc property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getRFC() {
                return rfc;
            }

            /**
             * Sets the value of the rfc property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setRFC(String value) {
                this.rfc = value;
            }

            /**
             * Gets the value of the genero property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getGenero() {
                return genero;
            }

            /**
             * Sets the value of the genero property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setGenero(String value) {
                this.genero = value;
            }

            /**
             * Gets the value of the elem property.
             *
             * @return
             *     possible object is
             *     {@link Integer }
             *
             */
            public Integer getElem() {
                return elem;
            }

            /**
             * Sets the value of the elem property.
             *
             * @param value
             *     allowed object is
             *     {@link Integer }
             *
             */
            public void setElem(Integer value) {
                this.elem = value;
            }

        }

    }

}
