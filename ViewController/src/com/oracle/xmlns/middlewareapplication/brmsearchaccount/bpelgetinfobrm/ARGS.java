
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "accountno" })
@XmlRootElement(name = "ARGS")
public class ARGS {

    @XmlElement(name = "ACCOUNT_NO", required = true)
    protected String accountno;
    @XmlAttribute(name = "elem")
    protected Integer elem;

    /**
     * Gets the value of the accountno property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getACCOUNTNO() {
        return accountno;
    }

    /**
     * Sets the value of the accountno property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setACCOUNTNO(String value) {
        this.accountno = value;
    }

    /**
     * Gets the value of the elem property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getElem() {
        return elem;
    }

    /**
     * Sets the value of the elem property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setElem(Integer value) {
        this.elem = value;
    }

}
