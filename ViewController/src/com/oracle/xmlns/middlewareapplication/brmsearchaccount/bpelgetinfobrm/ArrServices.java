
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Servicio" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ArrProduct"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                       &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                       &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ArrDiscount"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                       &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                       &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "servicio" })
@XmlRootElement(name = "ArrServices")
public class ArrServices {

    @XmlElement(name = "Servicio")
    protected List<ArrServices.Servicio> servicio;

    /**
     * Gets the value of the servicio property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicio property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicio().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrServices.Servicio }
     *
     *
     */
    public List<ArrServices.Servicio> getServicio() {
        if (servicio == null) {
            servicio = new ArrayList<ArrServices.Servicio>();
        }
        return this.servicio;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ArrProduct"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                             &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                             &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ArrDiscount"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                             &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                             &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder =
             { "service", "serialNumber", "mac", "dn", "incluidoFlag", "packageDescription", "packageId", "latitud",
               "longuitud", "creditLimit", "arrProduct", "arrDiscount"
        })
    public static class Servicio {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "SerialNumber", required = true)
        protected String serialNumber;
        @XmlElement(name = "Mac", required = true)
        protected String mac;
        @XmlElement(name = "DN", required = true)
        protected String dn;
        @XmlElement(name = "Incluido_Flag")
        protected int incluidoFlag;
        @XmlElement(name = "PackageDescription", required = true)
        protected String packageDescription;
        @XmlElement(name = "PackageId", required = true)
        protected String packageId;
        @XmlElement(name = "Latitud", required = true)
        protected String latitud;
        @XmlElement(name = "Longuitud", required = true)
        protected String longuitud;
        @XmlElement(name = "CreditLimit", required = true)
        protected String creditLimit;
        @XmlElement(name = "ArrProduct", required = true)
        protected ArrServices.Servicio.ArrProduct arrProduct;
        @XmlElement(name = "ArrDiscount", required = true)
        protected ArrServices.Servicio.ArrDiscount arrDiscount;

        /**
         * Gets the value of the service property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the serialNumber property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSerialNumber() {
            return serialNumber;
        }

        /**
         * Sets the value of the serialNumber property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSerialNumber(String value) {
            this.serialNumber = value;
        }

        /**
         * Gets the value of the mac property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getMac() {
            return mac;
        }

        /**
         * Sets the value of the mac property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setMac(String value) {
            this.mac = value;
        }

        /**
         * Gets the value of the dn property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDN() {
            return dn;
        }

        /**
         * Sets the value of the dn property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDN(String value) {
            this.dn = value;
        }

        /**
         * Gets the value of the incluidoFlag property.
         *
         */
        public int getIncluidoFlag() {
            return incluidoFlag;
        }

        /**
         * Sets the value of the incluidoFlag property.
         *
         */
        public void setIncluidoFlag(int value) {
            this.incluidoFlag = value;
        }

        /**
         * Gets the value of the packageDescription property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPackageDescription() {
            return packageDescription;
        }

        /**
         * Sets the value of the packageDescription property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPackageDescription(String value) {
            this.packageDescription = value;
        }

        /**
         * Gets the value of the packageId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPackageId() {
            return packageId;
        }

        /**
         * Sets the value of the packageId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPackageId(String value) {
            this.packageId = value;
        }

        /**
         * Gets the value of the latitud property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLatitud() {
            return latitud;
        }

        /**
         * Sets the value of the latitud property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLatitud(String value) {
            this.latitud = value;
        }

        /**
         * Gets the value of the longuitud property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLonguitud() {
            return longuitud;
        }

        /**
         * Sets the value of the longuitud property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLonguitud(String value) {
            this.longuitud = value;
        }

        /**
         * Gets the value of the creditLimit property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCreditLimit() {
            return creditLimit;
        }

        /**
         * Sets the value of the creditLimit property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCreditLimit(String value) {
            this.creditLimit = value;
        }

        /**
         * Gets the value of the arrProduct property.
         *
         * @return
         *     possible object is
         *     {@link ArrServices.Servicio.ArrProduct }
         *
         */
        public ArrServices.Servicio.ArrProduct getArrProduct() {
            return arrProduct;
        }

        /**
         * Sets the value of the arrProduct property.
         *
         * @param value
         *     allowed object is
         *     {@link ArrServices.Servicio.ArrProduct }
         *
         */
        public void setArrProduct(ArrServices.Servicio.ArrProduct value) {
            this.arrProduct = value;
        }

        /**
         * Gets the value of the arrDiscount property.
         *
         * @return
         *     possible object is
         *     {@link ArrServices.Servicio.ArrDiscount }
         *
         */
        public ArrServices.Servicio.ArrDiscount getArrDiscount() {
            return arrDiscount;
        }

        /**
         * Sets the value of the arrDiscount property.
         *
         * @param value
         *     allowed object is
         *     {@link ArrServices.Servicio.ArrDiscount }
         *
         */
        public void setArrDiscount(ArrServices.Servicio.ArrDiscount value) {
            this.arrDiscount = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "discount" })
        public static class ArrDiscount {

            @XmlElement(name = "Discount")
            protected List<ArrServices.Servicio.ArrDiscount.Discount> discount;

            /**
             * Gets the value of the discount property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the discount property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDiscount().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ArrServices.Servicio.ArrDiscount.Discount }
             *
             *
             */
            public List<ArrServices.Servicio.ArrDiscount.Discount> getDiscount() {
                if (discount == null) {
                    discount = new ArrayList<ArrServices.Servicio.ArrDiscount.Discount>();
                }
                return this.discount;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder =
                     { "idProduct", "startDate", "endDate", "quantity", "status", "additionalDescription", "packageId"
                })
            public static class Discount {

                @XmlElement(name = "IdProduct", required = true)
                protected String idProduct;
                @XmlElement(name = "StartDate", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar startDate;
                @XmlElement(name = "EndDate", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar endDate;
                @XmlElement(name = "Quantity", required = true)
                protected BigDecimal quantity;
                @XmlElement(name = "Status", required = true)
                protected String status;
                @XmlElement(name = "AdditionalDescription", required = true)
                protected String additionalDescription;
                @XmlElement(name = "PackageId", required = true)
                protected String packageId;

                /**
                 * Gets the value of the idProduct property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIdProduct() {
                    return idProduct;
                }

                /**
                 * Sets the value of the idProduct property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIdProduct(String value) {
                    this.idProduct = value;
                }

                /**
                 * Gets the value of the startDate property.
                 *
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public XMLGregorianCalendar getStartDate() {
                    return startDate;
                }

                /**
                 * Sets the value of the startDate property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public void setStartDate(XMLGregorianCalendar value) {
                    this.startDate = value;
                }

                /**
                 * Gets the value of the endDate property.
                 *
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public XMLGregorianCalendar getEndDate() {
                    return endDate;
                }

                /**
                 * Sets the value of the endDate property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public void setEndDate(XMLGregorianCalendar value) {
                    this.endDate = value;
                }

                /**
                 * Gets the value of the quantity property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getQuantity() {
                    return quantity;
                }

                /**
                 * Sets the value of the quantity property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setQuantity(BigDecimal value) {
                    this.quantity = value;
                }

                /**
                 * Gets the value of the status property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Sets the value of the status property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

                /**
                 * Gets the value of the additionalDescription property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getAdditionalDescription() {
                    return additionalDescription;
                }

                /**
                 * Sets the value of the additionalDescription property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setAdditionalDescription(String value) {
                    this.additionalDescription = value;
                }

                /**
                 * Gets the value of the packageId property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getPackageId() {
                    return packageId;
                }

                /**
                 * Sets the value of the packageId property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setPackageId(String value) {
                    this.packageId = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                   &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "product" })
        public static class ArrProduct {

            @XmlElement(name = "Product")
            protected List<ArrServices.Servicio.ArrProduct.Product> product;

            /**
             * Gets the value of the product property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the product property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getProduct().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ArrServices.Servicio.ArrProduct.Product }
             *
             *
             */
            public List<ArrServices.Servicio.ArrProduct.Product> getProduct() {
                if (product == null) {
                    product = new ArrayList<ArrServices.Servicio.ArrProduct.Product>();
                }
                return this.product;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder =
                     { "idProduct", "startDate", "endDate", "price", "discount", "quantity", "status",
                       "statusDescription", "additionalDescription", "packageId", "includeFlag"
                })
            public static class Product {

                @XmlElement(name = "IdProduct", required = true)
                protected String idProduct;
                @XmlElement(name = "StartDate", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar startDate;
                @XmlElement(name = "EndDate", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar endDate;
                @XmlElement(name = "Price", required = true)
                protected BigDecimal price;
                @XmlElement(name = "Discount", required = true)
                protected BigDecimal discount;
                @XmlElement(name = "Quantity", required = true)
                protected BigDecimal quantity;
                @XmlElement(name = "Status", required = true)
                protected String status;
                @XmlElement(name = "StatusDescription", required = true)
                protected String statusDescription;
                @XmlElement(name = "AdditionalDescription", required = true)
                protected String additionalDescription;
                @XmlElement(name = "PackageId", required = true)
                protected String packageId;
                @XmlElement(name = "IncludeFlag", required = true)
                protected String includeFlag;

                /**
                 * Gets the value of the idProduct property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIdProduct() {
                    return idProduct;
                }

                /**
                 * Sets the value of the idProduct property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIdProduct(String value) {
                    this.idProduct = value;
                }

                /**
                 * Gets the value of the startDate property.
                 *
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public XMLGregorianCalendar getStartDate() {
                    return startDate;
                }

                /**
                 * Sets the value of the startDate property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public void setStartDate(XMLGregorianCalendar value) {
                    this.startDate = value;
                }

                /**
                 * Gets the value of the endDate property.
                 *
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public XMLGregorianCalendar getEndDate() {
                    return endDate;
                }

                /**
                 * Sets the value of the endDate property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public void setEndDate(XMLGregorianCalendar value) {
                    this.endDate = value;
                }

                /**
                 * Gets the value of the price property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getPrice() {
                    return price;
                }

                /**
                 * Sets the value of the price property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setPrice(BigDecimal value) {
                    this.price = value;
                }

                /**
                 * Gets the value of the discount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getDiscount() {
                    return discount;
                }

                /**
                 * Sets the value of the discount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setDiscount(BigDecimal value) {
                    this.discount = value;
                }

                /**
                 * Gets the value of the quantity property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getQuantity() {
                    return quantity;
                }

                /**
                 * Sets the value of the quantity property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setQuantity(BigDecimal value) {
                    this.quantity = value;
                }

                /**
                 * Gets the value of the status property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Sets the value of the status property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

                /**
                 * Gets the value of the statusDescription property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getStatusDescription() {
                    return statusDescription;
                }

                /**
                 * Sets the value of the statusDescription property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setStatusDescription(String value) {
                    this.statusDescription = value;
                }

                /**
                 * Gets the value of the additionalDescription property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getAdditionalDescription() {
                    return additionalDescription;
                }

                /**
                 * Sets the value of the additionalDescription property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setAdditionalDescription(String value) {
                    this.additionalDescription = value;
                }

                /**
                 * Gets the value of the packageId property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getPackageId() {
                    return packageId;
                }

                /**
                 * Sets the value of the packageId property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setPackageId(String value) {
                    this.packageId = value;
                }

                /**
                 * Gets the value of the includeFlag property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIncludeFlag() {
                    return includeFlag;
                }

                /**
                 * Sets the value of the includeFlag property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIncludeFlag(String value) {
                    this.includeFlag = value;
                }

            }

        }

    }

}
