
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "bancoazteca", "bancomer", "hsbc", "banamex", "banorte", "scotiabank", "santander" })
@XmlRootElement(name = "ReferenciasBancarias")
public class ReferenciasBancarias {

    @XmlElement(name = "BANCO_AZTECA", required = true)
    protected String bancoazteca;
    @XmlElement(name = "BANCOMER", required = true)
    protected String bancomer;
    @XmlElement(name = "HSBC", required = true)
    protected String hsbc;
    @XmlElement(name = "BANAMEX", required = true)
    protected String banamex;
    @XmlElement(name = "BANORTE", required = true)
    protected String banorte;
    @XmlElement(name = "SCOTIABANK", required = true)
    protected String scotiabank;
    @XmlElement(name = "SANTANDER", required = true)
    protected String santander;

    /**
     * Gets the value of the bancoazteca property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBANCOAZTECA() {
        return bancoazteca;
    }

    /**
     * Sets the value of the bancoazteca property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBANCOAZTECA(String value) {
        this.bancoazteca = value;
    }

    /**
     * Gets the value of the bancomer property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBANCOMER() {
        return bancomer;
    }

    /**
     * Sets the value of the bancomer property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBANCOMER(String value) {
        this.bancomer = value;
    }

    /**
     * Gets the value of the hsbc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getHSBC() {
        return hsbc;
    }

    /**
     * Sets the value of the hsbc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setHSBC(String value) {
        this.hsbc = value;
    }

    /**
     * Gets the value of the banamex property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBANAMEX() {
        return banamex;
    }

    /**
     * Sets the value of the banamex property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBANAMEX(String value) {
        this.banamex = value;
    }

    /**
     * Gets the value of the banorte property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBANORTE() {
        return banorte;
    }

    /**
     * Sets the value of the banorte property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBANORTE(String value) {
        this.banorte = value;
    }

    /**
     * Gets the value of the scotiabank property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSCOTIABANK() {
        return scotiabank;
    }

    /**
     * Sets the value of the scotiabank property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSCOTIABANK(String value) {
        this.scotiabank = value;
    }

    /**
     * Gets the value of the santander property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSANTANDER() {
        return santander;
    }

    /**
     * Sets the value of the santander property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSANTANDER(String value) {
        this.santander = value;
    }

}
