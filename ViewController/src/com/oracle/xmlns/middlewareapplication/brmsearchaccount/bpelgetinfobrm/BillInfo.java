
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LastBillBalance"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SummaryStatement"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LastInvoiceSummary"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="historicoFacturacion"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "lastBillBalance", "summaryStatement", "lastInvoiceSummary", "historicoFacturacion" })
@XmlRootElement(name = "BillInfo")
public class BillInfo {

    @XmlElement(name = "LastBillBalance", required = true)
    protected BillInfo.LastBillBalance lastBillBalance;
    @XmlElement(name = "SummaryStatement", required = true)
    protected BillInfo.SummaryStatement summaryStatement;
    @XmlElement(name = "LastInvoiceSummary", required = true)
    protected BillInfo.LastInvoiceSummary lastInvoiceSummary;
    @XmlElement(required = true)
    protected BillInfo.HistoricoFacturacion historicoFacturacion;

    /**
     * Gets the value of the lastBillBalance property.
     *
     * @return
     *     possible object is
     *     {@link BillInfo.LastBillBalance }
     *
     */
    public BillInfo.LastBillBalance getLastBillBalance() {
        return lastBillBalance;
    }

    /**
     * Sets the value of the lastBillBalance property.
     *
     * @param value
     *     allowed object is
     *     {@link BillInfo.LastBillBalance }
     *
     */
    public void setLastBillBalance(BillInfo.LastBillBalance value) {
        this.lastBillBalance = value;
    }

    /**
     * Gets the value of the summaryStatement property.
     *
     * @return
     *     possible object is
     *     {@link BillInfo.SummaryStatement }
     *
     */
    public BillInfo.SummaryStatement getSummaryStatement() {
        return summaryStatement;
    }

    /**
     * Sets the value of the summaryStatement property.
     *
     * @param value
     *     allowed object is
     *     {@link BillInfo.SummaryStatement }
     *
     */
    public void setSummaryStatement(BillInfo.SummaryStatement value) {
        this.summaryStatement = value;
    }

    /**
     * Gets the value of the lastInvoiceSummary property.
     *
     * @return
     *     possible object is
     *     {@link BillInfo.LastInvoiceSummary }
     *
     */
    public BillInfo.LastInvoiceSummary getLastInvoiceSummary() {
        return lastInvoiceSummary;
    }

    /**
     * Sets the value of the lastInvoiceSummary property.
     *
     * @param value
     *     allowed object is
     *     {@link BillInfo.LastInvoiceSummary }
     *
     */
    public void setLastInvoiceSummary(BillInfo.LastInvoiceSummary value) {
        this.lastInvoiceSummary = value;
    }

    /**
     * Gets the value of the historicoFacturacion property.
     *
     * @return
     *     possible object is
     *     {@link BillInfo.HistoricoFacturacion }
     *
     */
    public BillInfo.HistoricoFacturacion getHistoricoFacturacion() {
        return historicoFacturacion;
    }

    /**
     * Sets the value of the historicoFacturacion property.
     *
     * @param value
     *     allowed object is
     *     {@link BillInfo.HistoricoFacturacion }
     *
     */
    public void setHistoricoFacturacion(BillInfo.HistoricoFacturacion value) {
        this.historicoFacturacion = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "factura" })
    public static class HistoricoFacturacion {

        @XmlElement(name = "Factura")
        protected List<BillInfo.HistoricoFacturacion.Factura> factura;

        /**
         * Gets the value of the factura property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the factura property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFactura().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BillInfo.HistoricoFacturacion.Factura }
         *
         *
         */
        public List<BillInfo.HistoricoFacturacion.Factura> getFactura() {
            if (factura == null) {
                factura = new ArrayList<BillInfo.HistoricoFacturacion.Factura>();
            }
            return this.factura;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "bilingDate", "amount" })
        public static class Factura {

            @XmlElement(required = true)
            protected String bilingDate;
            @XmlElement(required = true)
            protected String amount;

            /**
             * Gets the value of the bilingDate property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBilingDate() {
                return bilingDate;
            }

            /**
             * Sets the value of the bilingDate property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBilingDate(String value) {
                this.bilingDate = value;
            }

            /**
             * Gets the value of the amount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAmount() {
                return amount;
            }

            /**
             * Sets the value of the amount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAmount(String value) {
                this.amount = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder = { "totalPay", "currentBilling", "endT", "totalPaydiscount", "endTDiscount", "courtT" })
    public static class LastBillBalance {

        @XmlElement(required = true)
        protected BigDecimal totalPay;
        @XmlElement(required = true)
        protected BigDecimal currentBilling;
        @XmlElement(required = true)
        protected String endT;
        @XmlElement(required = true)
        protected BigDecimal totalPaydiscount;
        @XmlElement(required = true)
        protected BigDecimal endTDiscount;
        @XmlElement(required = true)
        protected String courtT;

        /**
         * Gets the value of the totalPay property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getTotalPay() {
            return totalPay;
        }

        /**
         * Sets the value of the totalPay property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setTotalPay(BigDecimal value) {
            this.totalPay = value;
        }

        /**
         * Gets the value of the currentBilling property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getCurrentBilling() {
            return currentBilling;
        }

        /**
         * Sets the value of the currentBilling property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setCurrentBilling(BigDecimal value) {
            this.currentBilling = value;
        }

        /**
         * Gets the value of the endT property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getEndT() {
            return endT;
        }

        /**
         * Sets the value of the endT property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setEndT(String value) {
            this.endT = value;
        }

        /**
         * Gets the value of the totalPaydiscount property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getTotalPaydiscount() {
            return totalPaydiscount;
        }

        /**
         * Sets the value of the totalPaydiscount property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setTotalPaydiscount(BigDecimal value) {
            this.totalPaydiscount = value;
        }

        /**
         * Gets the value of the endTDiscount property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getEndTDiscount() {
            return endTDiscount;
        }

        /**
         * Sets the value of the endTDiscount property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setEndTDiscount(BigDecimal value) {
            this.endTDiscount = value;
        }

        /**
         * Gets the value of the courtT property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCourtT() {
            return courtT;
        }

        /**
         * Sets the value of the courtT property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCourtT(String value) {
            this.courtT = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "period", "rent", "bonosesPromotion", "periodConsum", "subtotal", "totalBills" })
    public static class LastInvoiceSummary {

        @XmlElement(required = true)
        protected String period;
        @XmlElement(required = true)
        protected BigDecimal rent;
        @XmlElement(required = true)
        protected BigDecimal bonosesPromotion;
        @XmlElement(required = true)
        protected BigDecimal periodConsum;
        @XmlElement(required = true)
        protected BigDecimal subtotal;
        @XmlElement(required = true)
        protected BigDecimal totalBills;

        /**
         * Gets the value of the period property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPeriod() {
            return period;
        }

        /**
         * Sets the value of the period property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPeriod(String value) {
            this.period = value;
        }

        /**
         * Gets the value of the rent property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getRent() {
            return rent;
        }

        /**
         * Sets the value of the rent property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setRent(BigDecimal value) {
            this.rent = value;
        }

        /**
         * Gets the value of the bonosesPromotion property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getBonosesPromotion() {
            return bonosesPromotion;
        }

        /**
         * Sets the value of the bonosesPromotion property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setBonosesPromotion(BigDecimal value) {
            this.bonosesPromotion = value;
        }

        /**
         * Gets the value of the periodConsum property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getPeriodConsum() {
            return periodConsum;
        }

        /**
         * Sets the value of the periodConsum property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setPeriodConsum(BigDecimal value) {
            this.periodConsum = value;
        }

        /**
         * Gets the value of the subtotal property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getSubtotal() {
            return subtotal;
        }

        /**
         * Sets the value of the subtotal property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setSubtotal(BigDecimal value) {
            this.subtotal = value;
        }

        /**
         * Gets the value of the totalBills property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getTotalBills() {
            return totalBills;
        }

        /**
         * Sets the value of the totalBills property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setTotalBills(BigDecimal value) {
            this.totalBills = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "previiousBill", "payment", "creditNote", "currentBilling", "totalPay" })
    public static class SummaryStatement {

        @XmlElement(required = true)
        protected BigDecimal previiousBill;
        @XmlElement(required = true)
        protected BigDecimal payment;
        @XmlElement(required = true)
        protected BigDecimal creditNote;
        @XmlElement(required = true)
        protected BigDecimal currentBilling;
        @XmlElement(required = true)
        protected BigDecimal totalPay;

        /**
         * Gets the value of the previiousBill property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getPreviiousBill() {
            return previiousBill;
        }

        /**
         * Sets the value of the previiousBill property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setPreviiousBill(BigDecimal value) {
            this.previiousBill = value;
        }

        /**
         * Gets the value of the payment property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getPayment() {
            return payment;
        }

        /**
         * Sets the value of the payment property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setPayment(BigDecimal value) {
            this.payment = value;
        }

        /**
         * Gets the value of the creditNote property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getCreditNote() {
            return creditNote;
        }

        /**
         * Sets the value of the creditNote property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setCreditNote(BigDecimal value) {
            this.creditNote = value;
        }

        /**
         * Gets the value of the currentBilling property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getCurrentBilling() {
            return currentBilling;
        }

        /**
         * Sets the value of the currentBilling property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setCurrentBilling(BigDecimal value) {
            this.currentBilling = value;
        }

        /**
         * Gets the value of the totalPay property.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getTotalPay() {
            return totalPay;
        }

        /**
         * Sets the value of the totalPay property.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setTotalPay(BigDecimal value) {
            this.totalPay = value;
        }

    }

}
