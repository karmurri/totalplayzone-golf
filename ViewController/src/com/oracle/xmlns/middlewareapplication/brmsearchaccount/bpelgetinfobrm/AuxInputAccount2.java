
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FLAGS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TEMPLATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ARGS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "flags", "poid", "template", "args" })
@XmlRootElement(name = "AuxInputAccount2")
public class AuxInputAccount2 {

    @XmlElement(name = "FLAGS", required = true)
    protected String flags;
    @XmlElement(name = "POID", required = true)
    protected String poid;
    @XmlElement(name = "TEMPLATE", required = true)
    protected String template;
    @XmlElement(name = "ARGS", required = true)
    protected AuxInputAccount2.ARGS args;

    /**
     * Gets the value of the flags property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFLAGS() {
        return flags;
    }

    /**
     * Sets the value of the flags property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFLAGS(String value) {
        this.flags = value;
    }

    /**
     * Gets the value of the poid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPOID() {
        return poid;
    }

    /**
     * Sets the value of the poid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPOID(String value) {
        this.poid = value;
    }

    /**
     * Gets the value of the template property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTEMPLATE() {
        return template;
    }

    /**
     * Sets the value of the template property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTEMPLATE(String value) {
        this.template = value;
    }

    /**
     * Gets the value of the args property.
     *
     * @return
     *     possible object is
     *     {@link AuxInputAccount2 .ARGS }
     *
     */
    public AuxInputAccount2.ARGS getARGS() {
        return args;
    }

    /**
     * Sets the value of the args property.
     *
     * @param value
     *     allowed object is
     *     {@link AuxInputAccount2 .ARGS }
     *
     */
    public void setARGS(AuxInputAccount2.ARGS value) {
        this.args = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "accountobj" })
    public static class ARGS {

        @XmlElement(name = "ACCOUNT_OBJ", required = true)
        protected String accountobj;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the accountobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTOBJ() {
            return accountobj;
        }

        /**
         * Sets the value of the accountobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTOBJ(String value) {
            this.accountobj = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }

}
