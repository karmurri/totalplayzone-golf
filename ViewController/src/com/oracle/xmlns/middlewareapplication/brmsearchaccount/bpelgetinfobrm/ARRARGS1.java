
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ARGS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "args" })
@XmlRootElement(name = "ARR_ARGS1")
public class ARRARGS1 {

    @XmlElement(name = "ARGS", required = true)
    protected ARRARGS1.ARGS args;

    /**
     * Gets the value of the args property.
     *
     * @return
     *     possible object is
     *     {@link ARRARGS1 .ARGS }
     *
     */
    public ARRARGS1.ARGS getARGS() {
        return args;
    }

    /**
     * Sets the value of the args property.
     *
     * @param value
     *     allowed object is
     *     {@link ARRARGS1 .ARGS }
     *
     */
    public void setARGS(ARRARGS1.ARGS value) {
        this.args = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "accountobj" })
    public static class ARGS {

        @XmlElement(name = "ACCOUNT_OBJ", required = true)
        protected String accountobj;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the accountobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTOBJ() {
            return accountobj;
        }

        /**
         * Sets the value of the accountobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTOBJ(String value) {
            this.accountobj = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }

}
