
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessResponse }
     *
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link Deposito }
     *
     */
    public Deposito createDeposito() {
        return new Deposito();
    }

    /**
     * Create an instance of {@link AuxInputAccount }
     *
     */
    public AuxInputAccount createAuxInputAccount() {
        return new AuxInputAccount();
    }

    /**
     * Create an instance of {@link AuxInputAccount2 }
     *
     */
    public AuxInputAccount2 createAuxInputAccount2() {
        return new AuxInputAccount2();
    }

    /**
     * Create an instance of {@link AuxOutputAccount }
     *
     */
    public AuxOutputAccount createAuxOutputAccount() {
        return new AuxOutputAccount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress createArrAddress() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address createAddress() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address();
    }

    /**
     * Create an instance of {@link ARRARGS1 }
     *
     */
    public ARRARGS1 createARRARGS1() {
        return new ARRARGS1();
    }

    /**
     * Create an instance of {@link ARRARGS2 }
     *
     */
    public ARRARGS2 createARRARGS2() {
        return new ARRARGS2();
    }

    /**
     * Create an instance of {@link ARRPHONES }
     *
     */
    public ARRPHONES createARRPHONES() {
        return new ARRPHONES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio createServicio() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices createArrServices() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo createBillInfo() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo();
    }

    /**
     * Create an instance of {@link ArrPoidProduct }
     *
     */
    public ArrPoidProduct createArrPoidProduct() {
        return new ArrPoidProduct();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS createRESULTS() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES createRESULTSBALANCES() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion createBillInfoHistoricoFacturacion() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio createArrServicesServicio() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount createArrServicesServicioArrDiscount() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct createArrServicesServicioArrProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount createServicioArrDiscount() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct createServicioArrProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address createArrAddressAddress() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address();
    }

    /**
     * Create an instance of {@link AuxOutputAccount.RESULTS }
     *
     */
    public AuxOutputAccount.RESULTS createAuxOutputAccountRESULTS() {
        return new AuxOutputAccount.RESULTS();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM }
     *
     */
    public ProcessResponse.ArrBRM createProcessResponseArrBRM() {
        return new ProcessResponse.ArrBRM();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo createProcessResponseArrBRMBillInfo() {
        return new ProcessResponse.ArrBRM.BillInfo();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion createProcessResponseArrBRMBillInfoHistoricoFacturacion() {
        return new ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.Last3Payments }
     *
     */
    public ProcessResponse.ArrBRM.Last3Payments createProcessResponseArrBRMLast3Payments() {
        return new ProcessResponse.ArrBRM.Last3Payments();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices createProcessResponseArrBRMArrServices() {
        return new ProcessResponse.ArrBRM.ArrServices();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices.Servicio }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices.Servicio createProcessResponseArrBRMArrServicesServicio() {
        return new ProcessResponse.ArrBRM.ArrServices.Servicio();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount createProcessResponseArrBRMArrServicesServicioArrDiscount() {
        return new ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct createProcessResponseArrBRMArrServicesServicioArrProduct() {
        return new ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrAddress }
     *
     */
    public ProcessResponse.ArrBRM.ArrAddress createProcessResponseArrBRMArrAddress() {
        return new ProcessResponse.ArrBRM.ArrAddress();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrAddress.Address }
     *
     */
    public ProcessResponse.ArrBRM.ArrAddress.Address createProcessResponseArrBRMArrAddressAddress() {
        return new ProcessResponse.ArrBRM.ArrAddress.Address();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrPayType }
     *
     */
    public ProcessResponse.ArrBRM.ArrPayType createProcessResponseArrBRMArrPayType() {
        return new ProcessResponse.ArrBRM.ArrPayType();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo }
     *
     */
    public ProcessResponse.ArrBRM.ArrPayType.PayInfo createProcessResponseArrBRMArrPayTypePayInfo() {
        return new ProcessResponse.ArrBRM.ArrPayType.PayInfo();
    }

    /**
     * Create an instance of {@link Process }
     *
     */
    public Process createProcess() {
        return new Process();
    }

    /**
     * Create an instance of {@link Deposito.DepositoG }
     *
     */
    public Deposito.DepositoG createDepositoDepositoG() {
        return new Deposito.DepositoG();
    }

    /**
     * Create an instance of {@link AuxInputAccount.ARGS }
     *
     */
    public AuxInputAccount.ARGS createAuxInputAccountARGS() {
        return new AuxInputAccount.ARGS();
    }

    /**
     * Create an instance of {@link AuxInputAccount2 .ARGS }
     *
     */
    public AuxInputAccount2.ARGS createAuxInputAccount2ARGS() {
        return new AuxInputAccount2.ARGS();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address.PHONES }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address.PHONES createAddressPHONES() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Address.PHONES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ARGS }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ARGS createARGS() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ARGS();
    }

    /**
     * Create an instance of {@link ARRARGS1 .ARGS }
     *
     */
    public ARRARGS1.ARGS createARRARGS1ARGS() {
        return new ARRARGS1.ARGS();
    }

    /**
     * Create an instance of {@link ARRARGS2 .ARGS }
     *
     */
    public ARRARGS2.ARGS createARRARGS2ARGS() {
        return new ARRARGS2.ARGS();
    }

    /**
     * Create an instance of {@link ARRPHONES.PHONES }
     *
     */
    public ARRPHONES.PHONES createARRPHONESPHONES() {
        return new ARRPHONES.PHONES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PHONES }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PHONES createPHONES() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PHONES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Payment }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Payment createPayment() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Payment();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Product }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Product createProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Product();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Discount }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Discount createDiscount() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Discount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastBillBalance }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastBillBalance createBillInfoLastBillBalance() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastBillBalance();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.SummaryStatement }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.SummaryStatement createBillInfoSummaryStatement() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.SummaryStatement();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastInvoiceSummary }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastInvoiceSummary createBillInfoLastInvoiceSummary() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.LastInvoiceSummary();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ReferenciasBancarias }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ReferenciasBancarias createReferenciasBancarias() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ReferenciasBancarias();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Factura }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Factura createFactura() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Factura();
    }

    /**
     * Create an instance of {@link ArrPoidProduct.PoidProduct }
     *
     */
    public ArrPoidProduct.PoidProduct createArrPoidProductPoidProduct() {
        return new ArrPoidProduct.PoidProduct();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PoidProduct }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PoidProduct createPoidProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.PoidProduct();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES.SUBBALANCES }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES.SUBBALANCES createRESULTSBALANCESSUBBALANCES() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.RESULTS.BALANCES.SUBBALANCES();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion.Factura }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion.Factura createBillInfoHistoricoFacturacionFactura() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.BillInfo.HistoricoFacturacion.Factura();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount.Discount }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount.Discount createArrServicesServicioArrDiscountDiscount() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrDiscount.Discount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct.Product }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct.Product createArrServicesServicioArrProductProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrServices.Servicio.ArrProduct.Product();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount.Discount }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount.Discount createServicioArrDiscountDiscount() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrDiscount.Discount();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct.Product }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct.Product createServicioArrProductProduct() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.Servicio.ArrProduct.Product();
    }

    /**
     * Create an instance of {@link com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address.PHONES }
     *
     */
    public com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address.PHONES createArrAddressAddressPHONES() {
        return new com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm.ArrAddress.Address.PHONES();
    }

    /**
     * Create an instance of {@link AuxOutputAccount.RESULTS.NAMEINFO }
     *
     */
    public AuxOutputAccount.RESULTS.NAMEINFO createAuxOutputAccountRESULTSNAMEINFO() {
        return new AuxOutputAccount.RESULTS.NAMEINFO();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.Contract }
     *
     */
    public ProcessResponse.ArrBRM.Contract createProcessResponseArrBRMContract() {
        return new ProcessResponse.ArrBRM.Contract();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ReferenciasBancarias }
     *
     */
    public ProcessResponse.ArrBRM.ReferenciasBancarias createProcessResponseArrBRMReferenciasBancarias() {
        return new ProcessResponse.ArrBRM.ReferenciasBancarias();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo.LastBillBalance }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo.LastBillBalance createProcessResponseArrBRMBillInfoLastBillBalance() {
        return new ProcessResponse.ArrBRM.BillInfo.LastBillBalance();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo.SummaryStatement }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo.SummaryStatement createProcessResponseArrBRMBillInfoSummaryStatement() {
        return new ProcessResponse.ArrBRM.BillInfo.SummaryStatement();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary createProcessResponseArrBRMBillInfoLastInvoiceSummary() {
        return new ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura }
     *
     */
    public ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura createProcessResponseArrBRMBillInfoHistoricoFacturacionFactura() {
        return new ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.Last3Payments.Payment }
     *
     */
    public ProcessResponse.ArrBRM.Last3Payments.Payment createProcessResponseArrBRMLast3PaymentsPayment() {
        return new ProcessResponse.ArrBRM.Last3Payments.Payment();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount createProcessResponseArrBRMArrServicesServicioArrDiscountDiscount() {
        return new ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product }
     *
     */
    public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product createProcessResponseArrBRMArrServicesServicioArrProductProduct() {
        return new ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrAddress.Address.PHONES }
     *
     */
    public ProcessResponse.ArrBRM.ArrAddress.Address.PHONES createProcessResponseArrBRMArrAddressAddressPHONES() {
        return new ProcessResponse.ArrBRM.ArrAddress.Address.PHONES();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv }
     *
     */
    public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv createProcessResponseArrBRMArrPayTypePayInfoArrInv() {
        return new ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD }
     *
     */
    public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD createProcessResponseArrBRMArrPayTypePayInfoArrDD() {
        return new ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD();
    }

    /**
     * Create an instance of {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC }
     *
     */
    public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC createProcessResponseArrBRMArrPayTypePayInfoArrCC() {
        return new ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC();
    }

}
