
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PHONES" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
         propOrder =
         { "id", "idDescription", "street", "numExterior", "numInterior", "city", "colonia", "delegacion",
           "entreCalles", "latitud", "longitud", "country", "email", "state", "zip", "title", "firstName", "middleName",
           "lastName", "birthday", "razonSocial", "rfc", "genero", "phones"
    })
@XmlRootElement(name = "Address")
public class Address {

    @XmlElement(name = "Id", required = true)
    protected String id;
    @XmlElement(name = "IdDescription", required = true)
    protected String idDescription;
    @XmlElement(name = "Street", required = true)
    protected String street;
    @XmlElement(name = "NumExterior", required = true)
    protected String numExterior;
    @XmlElement(name = "NumInterior", required = true)
    protected String numInterior;
    @XmlElement(name = "City", required = true)
    protected String city;
    @XmlElement(name = "Colonia", required = true)
    protected String colonia;
    @XmlElement(name = "Delegacion", required = true)
    protected String delegacion;
    @XmlElement(name = "EntreCalles", required = true)
    protected String entreCalles;
    @XmlElement(name = "Latitud", required = true)
    protected String latitud;
    @XmlElement(name = "Longitud", required = true)
    protected String longitud;
    @XmlElement(name = "Country", required = true)
    protected String country;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(name = "State", required = true)
    protected String state;
    @XmlElement(name = "Zip", required = true)
    protected String zip;
    @XmlElement(name = "Title", required = true)
    protected String title;
    @XmlElement(name = "FirstName", required = true)
    protected String firstName;
    @XmlElement(name = "MiddleName", required = true)
    protected String middleName;
    @XmlElement(name = "LastName", required = true)
    protected String lastName;
    @XmlElement(name = "Birthday", required = true)
    protected String birthday;
    @XmlElement(name = "RazonSocial", required = true)
    protected String razonSocial;
    @XmlElement(name = "RFC", required = true)
    protected String rfc;
    @XmlElement(name = "Genero", required = true)
    protected String genero;
    @XmlElement(name = "PHONES", required = true)
    protected List<Address.PHONES> phones;

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the idDescription property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdDescription() {
        return idDescription;
    }

    /**
     * Sets the value of the idDescription property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdDescription(String value) {
        this.idDescription = value;
    }

    /**
     * Gets the value of the street property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the numExterior property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumExterior() {
        return numExterior;
    }

    /**
     * Sets the value of the numExterior property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumExterior(String value) {
        this.numExterior = value;
    }

    /**
     * Gets the value of the numInterior property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumInterior() {
        return numInterior;
    }

    /**
     * Sets the value of the numInterior property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumInterior(String value) {
        this.numInterior = value;
    }

    /**
     * Gets the value of the city property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the colonia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getColonia() {
        return colonia;
    }

    /**
     * Sets the value of the colonia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setColonia(String value) {
        this.colonia = value;
    }

    /**
     * Gets the value of the delegacion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDelegacion() {
        return delegacion;
    }

    /**
     * Sets the value of the delegacion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDelegacion(String value) {
        this.delegacion = value;
    }

    /**
     * Gets the value of the entreCalles property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEntreCalles() {
        return entreCalles;
    }

    /**
     * Sets the value of the entreCalles property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEntreCalles(String value) {
        this.entreCalles = value;
    }

    /**
     * Gets the value of the latitud property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * Sets the value of the latitud property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLatitud(String value) {
        this.latitud = value;
    }

    /**
     * Gets the value of the longitud property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * Sets the value of the longitud property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLongitud(String value) {
        this.longitud = value;
    }

    /**
     * Gets the value of the country property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the email property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zip property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the title property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the birthday property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Sets the value of the birthday property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBirthday(String value) {
        this.birthday = value;
    }

    /**
     * Gets the value of the razonSocial property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * Sets the value of the razonSocial property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRazonSocial(String value) {
        this.razonSocial = value;
    }

    /**
     * Gets the value of the rfc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRFC() {
        return rfc;
    }

    /**
     * Sets the value of the rfc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRFC(String value) {
        this.rfc = value;
    }

    /**
     * Gets the value of the genero property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGenero() {
        return genero;
    }

    /**
     * Sets the value of the genero property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGenero(String value) {
        this.genero = value;
    }

    /**
     * Gets the value of the phones property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the phones property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPHONES().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address.PHONES }
     *
     *
     */
    public List<Address.PHONES> getPHONES() {
        if (phones == null) {
            phones = new ArrayList<Address.PHONES>();
        }
        return this.phones;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "phone", "type", "description" })
    public static class PHONES {

        @XmlElement(name = "PHONE", required = true)
        protected String phone;
        @XmlElement(name = "TYPE", required = true)
        protected String type;
        @XmlElement(required = true)
        protected String description;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the phone property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPHONE() {
            return phone;
        }

        /**
         * Sets the value of the phone property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPHONE(String value) {
            this.phone = value;
        }

        /**
         * Gets the value of the type property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTYPE() {
            return type;
        }

        /**
         * Sets the value of the type property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTYPE(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the description property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }

}
