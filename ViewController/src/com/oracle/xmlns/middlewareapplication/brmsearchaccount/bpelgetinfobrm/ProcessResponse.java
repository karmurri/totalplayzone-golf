
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResultDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ArrBRM"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BusinessType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BusinessTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CurrencyDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PayTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ArrPayType" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="InvType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="InvTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="PayInfo" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ArrInv" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="ArrDD" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="ArrCC" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Cluster" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ContractType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CiaPerteneciente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="renta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CreditLimitIptv" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CreditLimitTelephony" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ArrAddress"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Address" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="PHONES" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ArrServices"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Servicio" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                                       &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="ArrProduct"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                                           &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                                           &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                                           &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                                           &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                                           &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="ArrDiscount"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                                           &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                                                           &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                                           &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                           &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Contract"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Last3Payments"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Payment" maxOccurs="3"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="BillInfo"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="LastBillBalance"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="SummaryStatement"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="LastInvoiceSummary"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="historicoFacturacion"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ReferenciasBancarias"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "idResult", "result", "resultDescription", "arrBRM" })
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "IdResult", required = true)
    protected String idResult;
    @XmlElement(name = "Result", required = true)
    protected String result;
    @XmlElement(name = "ResultDescription", required = true)
    protected String resultDescription;
    @XmlElement(name = "ArrBRM", required = true)
    protected ProcessResponse.ArrBRM arrBRM;

    /**
     * Gets the value of the idResult property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdResult() {
        return idResult;
    }

    /**
     * Sets the value of the idResult property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdResult(String value) {
        this.idResult = value;
    }

    /**
     * Gets the value of the result property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the resultDescription property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getResultDescription() {
        return resultDescription;
    }

    /**
     * Sets the value of the resultDescription property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setResultDescription(String value) {
        this.resultDescription = value;
    }

    /**
     * Gets the value of the arrBRM property.
     *
     * @return
     *     possible object is
     *     {@link ProcessResponse.ArrBRM }
     *
     */
    public ProcessResponse.ArrBRM getArrBRM() {
        return arrBRM;
    }

    /**
     * Sets the value of the arrBRM property.
     *
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.ArrBRM }
     *
     */
    public void setArrBRM(ProcessResponse.ArrBRM value) {
        this.arrBRM = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BusinessType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BusinessTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CurrencyDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PayTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ArrPayType" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="InvType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="InvTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="PayInfo" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ArrInv" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="ArrDD" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="ArrCC" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Cluster" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ContractType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CiaPerteneciente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="renta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CreditLimitIptv" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CreditLimitTelephony" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ArrAddress"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Address" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="PHONES" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ArrServices"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Servicio" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *                             &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="ArrProduct"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                                                 &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                                                 &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                                                 &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                                                 &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                                                 &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="ArrDiscount"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                                                 &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                                                 &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                                                 &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                                 &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Contract"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Last3Payments"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Payment" maxOccurs="3"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="BillInfo"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="LastBillBalance"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="SummaryStatement"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="LastInvoiceSummary"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="historicoFacturacion"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                       &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ReferenciasBancarias"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
             propOrder =
             { "accountno", "businessType", "businessTypeDescription", "currency", "currencyDescription", "payType",
               "payTypeDescription", "arrPayType", "cluster", "packageDescription", "name", "status", "contractType",
               "ciaPerteneciente", "renta", "creditLimitIptv", "creditLimitTelephony", "arrAddress", "arrServices",
               "contract", "last3Payments", "billInfo", "referenciasBancarias"
        })
    public static class ArrBRM {

        @XmlElement(name = "ACCOUNT_NO", required = true)
        protected String accountno;
        @XmlElement(name = "BusinessType", required = true)
        protected String businessType;
        @XmlElement(name = "BusinessTypeDescription", required = true)
        protected String businessTypeDescription;
        @XmlElement(name = "Currency", required = true)
        protected String currency;
        @XmlElement(name = "CurrencyDescription", required = true)
        protected String currencyDescription;
        @XmlElement(name = "PayType", required = true)
        protected String payType;
        @XmlElement(name = "PayTypeDescription", required = true)
        protected String payTypeDescription;
        @XmlElement(name = "ArrPayType")
        protected List<ProcessResponse.ArrBRM.ArrPayType> arrPayType;
        @XmlElement(name = "Cluster", required = true)
        protected String cluster;
        @XmlElement(name = "PackageDescription", required = true)
        protected String packageDescription;
        @XmlElement(name = "Name", required = true)
        protected String name;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "ContractType", required = true)
        protected String contractType;
        @XmlElement(name = "CiaPerteneciente", required = true)
        protected String ciaPerteneciente;
        @XmlElement(required = true)
        protected String renta;
        @XmlElement(name = "CreditLimitIptv", required = true)
        protected String creditLimitIptv;
        @XmlElement(name = "CreditLimitTelephony", required = true)
        protected String creditLimitTelephony;
        @XmlElement(name = "ArrAddress", required = true)
        protected ProcessResponse.ArrBRM.ArrAddress arrAddress;
        @XmlElement(name = "ArrServices", required = true)
        protected ProcessResponse.ArrBRM.ArrServices arrServices;
        @XmlElement(name = "Contract", required = true)
        protected ProcessResponse.ArrBRM.Contract contract;
        @XmlElement(name = "Last3Payments", required = true)
        protected ProcessResponse.ArrBRM.Last3Payments last3Payments;
        @XmlElement(name = "BillInfo", required = true)
        protected ProcessResponse.ArrBRM.BillInfo billInfo;
        @XmlElement(name = "ReferenciasBancarias", required = true)
        protected ProcessResponse.ArrBRM.ReferenciasBancarias referenciasBancarias;

        /**
         * Gets the value of the accountno property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTNO() {
            return accountno;
        }

        /**
         * Sets the value of the accountno property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTNO(String value) {
            this.accountno = value;
        }

        /**
         * Gets the value of the businessType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBusinessType() {
            return businessType;
        }

        /**
         * Sets the value of the businessType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBusinessType(String value) {
            this.businessType = value;
        }

        /**
         * Gets the value of the businessTypeDescription property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBusinessTypeDescription() {
            return businessTypeDescription;
        }

        /**
         * Sets the value of the businessTypeDescription property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBusinessTypeDescription(String value) {
            this.businessTypeDescription = value;
        }

        /**
         * Gets the value of the currency property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCurrency(String value) {
            this.currency = value;
        }

        /**
         * Gets the value of the currencyDescription property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCurrencyDescription() {
            return currencyDescription;
        }

        /**
         * Sets the value of the currencyDescription property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCurrencyDescription(String value) {
            this.currencyDescription = value;
        }

        /**
         * Gets the value of the payType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPayType() {
            return payType;
        }

        /**
         * Sets the value of the payType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPayType(String value) {
            this.payType = value;
        }

        /**
         * Gets the value of the payTypeDescription property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPayTypeDescription() {
            return payTypeDescription;
        }

        /**
         * Sets the value of the payTypeDescription property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPayTypeDescription(String value) {
            this.payTypeDescription = value;
        }

        /**
         * Gets the value of the arrPayType property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the arrPayType property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getArrPayType().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessResponse.ArrBRM.ArrPayType }
         *
         *
         */
        public List<ProcessResponse.ArrBRM.ArrPayType> getArrPayType() {
            if (arrPayType == null) {
                arrPayType = new ArrayList<ProcessResponse.ArrBRM.ArrPayType>();
            }
            return this.arrPayType;
        }

        /**
         * Gets the value of the cluster property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCluster() {
            return cluster;
        }

        /**
         * Sets the value of the cluster property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCluster(String value) {
            this.cluster = value;
        }

        /**
         * Gets the value of the packageDescription property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPackageDescription() {
            return packageDescription;
        }

        /**
         * Sets the value of the packageDescription property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPackageDescription(String value) {
            this.packageDescription = value;
        }

        /**
         * Gets the value of the name property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the status property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the contractType property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getContractType() {
            return contractType;
        }

        /**
         * Sets the value of the contractType property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setContractType(String value) {
            this.contractType = value;
        }

        /**
         * Gets the value of the ciaPerteneciente property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCiaPerteneciente() {
            return ciaPerteneciente;
        }

        /**
         * Sets the value of the ciaPerteneciente property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCiaPerteneciente(String value) {
            this.ciaPerteneciente = value;
        }

        /**
         * Gets the value of the renta property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRenta() {
            return renta;
        }

        /**
         * Sets the value of the renta property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRenta(String value) {
            this.renta = value;
        }

        /**
         * Gets the value of the creditLimitIptv property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCreditLimitIptv() {
            return creditLimitIptv;
        }

        /**
         * Sets the value of the creditLimitIptv property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCreditLimitIptv(String value) {
            this.creditLimitIptv = value;
        }

        /**
         * Gets the value of the creditLimitTelephony property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCreditLimitTelephony() {
            return creditLimitTelephony;
        }

        /**
         * Sets the value of the creditLimitTelephony property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCreditLimitTelephony(String value) {
            this.creditLimitTelephony = value;
        }

        /**
         * Gets the value of the arrAddress property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.ArrAddress }
         *
         */
        public ProcessResponse.ArrBRM.ArrAddress getArrAddress() {
            return arrAddress;
        }

        /**
         * Sets the value of the arrAddress property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.ArrAddress }
         *
         */
        public void setArrAddress(ProcessResponse.ArrBRM.ArrAddress value) {
            this.arrAddress = value;
        }

        /**
         * Gets the value of the arrServices property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.ArrServices }
         *
         */
        public ProcessResponse.ArrBRM.ArrServices getArrServices() {
            return arrServices;
        }

        /**
         * Sets the value of the arrServices property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.ArrServices }
         *
         */
        public void setArrServices(ProcessResponse.ArrBRM.ArrServices value) {
            this.arrServices = value;
        }

        /**
         * Gets the value of the contract property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.Contract }
         *
         */
        public ProcessResponse.ArrBRM.Contract getContract() {
            return contract;
        }

        /**
         * Sets the value of the contract property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.Contract }
         *
         */
        public void setContract(ProcessResponse.ArrBRM.Contract value) {
            this.contract = value;
        }

        /**
         * Gets the value of the last3Payments property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.Last3Payments }
         *
         */
        public ProcessResponse.ArrBRM.Last3Payments getLast3Payments() {
            return last3Payments;
        }

        /**
         * Sets the value of the last3Payments property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.Last3Payments }
         *
         */
        public void setLast3Payments(ProcessResponse.ArrBRM.Last3Payments value) {
            this.last3Payments = value;
        }

        /**
         * Gets the value of the billInfo property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.BillInfo }
         *
         */
        public ProcessResponse.ArrBRM.BillInfo getBillInfo() {
            return billInfo;
        }

        /**
         * Sets the value of the billInfo property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.BillInfo }
         *
         */
        public void setBillInfo(ProcessResponse.ArrBRM.BillInfo value) {
            this.billInfo = value;
        }

        /**
         * Gets the value of the referenciasBancarias property.
         *
         * @return
         *     possible object is
         *     {@link ProcessResponse.ArrBRM.ReferenciasBancarias }
         *
         */
        public ProcessResponse.ArrBRM.ReferenciasBancarias getReferenciasBancarias() {
            return referenciasBancarias;
        }

        /**
         * Sets the value of the referenciasBancarias property.
         *
         * @param value
         *     allowed object is
         *     {@link ProcessResponse.ArrBRM.ReferenciasBancarias }
         *
         */
        public void setReferenciasBancarias(ProcessResponse.ArrBRM.ReferenciasBancarias value) {
            this.referenciasBancarias = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Address" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="PHONES" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "address" })
        public static class ArrAddress {

            @XmlElement(name = "Address", required = true)
            protected List<ProcessResponse.ArrBRM.ArrAddress.Address> address;

            /**
             * Gets the value of the address property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the address property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddress().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProcessResponse.ArrBRM.ArrAddress.Address }
             *
             *
             */
            public List<ProcessResponse.ArrBRM.ArrAddress.Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<ProcessResponse.ArrBRM.ArrAddress.Address>();
                }
                return this.address;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="IdDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="NumExterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="NumInterior" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Colonia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Delegacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="EntreCalles" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Longitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RazonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RFC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Genero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="PHONES" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder =
                     { "id", "idDescription", "street", "numExterior", "numInterior", "city", "colonia", "delegacion",
                       "entreCalles", "latitud", "longitud", "country", "email", "state", "zip", "title", "firstName",
                       "middleName", "lastName", "birthday", "razonSocial", "rfc", "genero", "phones"
                })
            public static class Address {

                @XmlElement(name = "Id", required = true)
                protected String id;
                @XmlElement(name = "IdDescription", required = true)
                protected String idDescription;
                @XmlElement(name = "Street", required = true)
                protected String street;
                @XmlElement(name = "NumExterior", required = true)
                protected String numExterior;
                @XmlElement(name = "NumInterior", required = true)
                protected String numInterior;
                @XmlElement(name = "City", required = true)
                protected String city;
                @XmlElement(name = "Colonia", required = true)
                protected String colonia;
                @XmlElement(name = "Delegacion", required = true)
                protected String delegacion;
                @XmlElement(name = "EntreCalles", required = true)
                protected String entreCalles;
                @XmlElement(name = "Latitud", required = true)
                protected String latitud;
                @XmlElement(name = "Longitud", required = true)
                protected String longitud;
                @XmlElement(name = "Country", required = true)
                protected String country;
                @XmlElement(required = true)
                protected String email;
                @XmlElement(name = "State", required = true)
                protected String state;
                @XmlElement(name = "Zip", required = true)
                protected String zip;
                @XmlElement(name = "Title", required = true)
                protected String title;
                @XmlElement(name = "FirstName", required = true)
                protected String firstName;
                @XmlElement(name = "MiddleName", required = true)
                protected String middleName;
                @XmlElement(name = "LastName", required = true)
                protected String lastName;
                @XmlElement(name = "Birthday", required = true)
                protected String birthday;
                @XmlElement(name = "RazonSocial", required = true)
                protected String razonSocial;
                @XmlElement(name = "RFC", required = true)
                protected String rfc;
                @XmlElement(name = "Genero", required = true)
                protected String genero;
                @XmlElement(name = "PHONES", required = true)
                protected List<ProcessResponse.ArrBRM.ArrAddress.Address.PHONES> phones;

                /**
                 * Gets the value of the id property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setId(String value) {
                    this.id = value;
                }

                /**
                 * Gets the value of the idDescription property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIdDescription() {
                    return idDescription;
                }

                /**
                 * Sets the value of the idDescription property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIdDescription(String value) {
                    this.idDescription = value;
                }

                /**
                 * Gets the value of the street property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getStreet() {
                    return street;
                }

                /**
                 * Sets the value of the street property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setStreet(String value) {
                    this.street = value;
                }

                /**
                 * Gets the value of the numExterior property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getNumExterior() {
                    return numExterior;
                }

                /**
                 * Sets the value of the numExterior property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setNumExterior(String value) {
                    this.numExterior = value;
                }

                /**
                 * Gets the value of the numInterior property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getNumInterior() {
                    return numInterior;
                }

                /**
                 * Sets the value of the numInterior property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setNumInterior(String value) {
                    this.numInterior = value;
                }

                /**
                 * Gets the value of the city property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCity() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCity(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the colonia property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getColonia() {
                    return colonia;
                }

                /**
                 * Sets the value of the colonia property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setColonia(String value) {
                    this.colonia = value;
                }

                /**
                 * Gets the value of the delegacion property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getDelegacion() {
                    return delegacion;
                }

                /**
                 * Sets the value of the delegacion property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setDelegacion(String value) {
                    this.delegacion = value;
                }

                /**
                 * Gets the value of the entreCalles property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEntreCalles() {
                    return entreCalles;
                }

                /**
                 * Sets the value of the entreCalles property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEntreCalles(String value) {
                    this.entreCalles = value;
                }

                /**
                 * Gets the value of the latitud property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getLatitud() {
                    return latitud;
                }

                /**
                 * Sets the value of the latitud property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setLatitud(String value) {
                    this.latitud = value;
                }

                /**
                 * Gets the value of the longitud property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getLongitud() {
                    return longitud;
                }

                /**
                 * Sets the value of the longitud property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setLongitud(String value) {
                    this.longitud = value;
                }

                /**
                 * Gets the value of the country property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCountry() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCountry(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the email property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the state property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getState() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setState(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the zip property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getZip() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setZip(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the title property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getTitle() {
                    return title;
                }

                /**
                 * Sets the value of the title property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setTitle(String value) {
                    this.title = value;
                }

                /**
                 * Gets the value of the firstName property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getFirstName() {
                    return firstName;
                }

                /**
                 * Sets the value of the firstName property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setFirstName(String value) {
                    this.firstName = value;
                }

                /**
                 * Gets the value of the middleName property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getMiddleName() {
                    return middleName;
                }

                /**
                 * Sets the value of the middleName property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setMiddleName(String value) {
                    this.middleName = value;
                }

                /**
                 * Gets the value of the lastName property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getLastName() {
                    return lastName;
                }

                /**
                 * Sets the value of the lastName property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setLastName(String value) {
                    this.lastName = value;
                }

                /**
                 * Gets the value of the birthday property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getBirthday() {
                    return birthday;
                }

                /**
                 * Sets the value of the birthday property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setBirthday(String value) {
                    this.birthday = value;
                }

                /**
                 * Gets the value of the razonSocial property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getRazonSocial() {
                    return razonSocial;
                }

                /**
                 * Sets the value of the razonSocial property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setRazonSocial(String value) {
                    this.razonSocial = value;
                }

                /**
                 * Gets the value of the rfc property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getRFC() {
                    return rfc;
                }

                /**
                 * Sets the value of the rfc property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setRFC(String value) {
                    this.rfc = value;
                }

                /**
                 * Gets the value of the genero property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getGenero() {
                    return genero;
                }

                /**
                 * Sets the value of the genero property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setGenero(String value) {
                    this.genero = value;
                }

                /**
                 * Gets the value of the phones property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the phones property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPHONES().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ProcessResponse.ArrBRM.ArrAddress.Address.PHONES }
                 *
                 *
                 */
                public List<ProcessResponse.ArrBRM.ArrAddress.Address.PHONES> getPHONES() {
                    if (phones == null) {
                        phones = new ArrayList<ProcessResponse.ArrBRM.ArrAddress.Address.PHONES>();
                    }
                    return this.phones;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = { "phone", "type", "description" })
                public static class PHONES {

                    @XmlElement(name = "PHONE", required = true)
                    protected String phone;
                    @XmlElement(name = "TYPE", required = true)
                    protected String type;
                    @XmlElement(required = true)
                    protected String description;
                    @XmlAttribute(name = "elem")
                    protected Integer elem;

                    /**
                     * Gets the value of the phone property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getPHONE() {
                        return phone;
                    }

                    /**
                     * Sets the value of the phone property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setPHONE(String value) {
                        this.phone = value;
                    }

                    /**
                     * Gets the value of the type property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getTYPE() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setTYPE(String value) {
                        this.type = value;
                    }

                    /**
                     * Gets the value of the description property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getDescription() {
                        return description;
                    }

                    /**
                     * Sets the value of the description property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setDescription(String value) {
                        this.description = value;
                    }

                    /**
                     * Gets the value of the elem property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *
                     */
                    public Integer getElem() {
                        return elem;
                    }

                    /**
                     * Sets the value of the elem property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *
                     */
                    public void setElem(Integer value) {
                        this.elem = value;
                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="InvType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="InvTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="PayInfo" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ArrInv" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="ArrDD" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="ArrCC" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "invType", "invTypeDescription", "payInfo" })
        public static class ArrPayType {

            @XmlElement(name = "InvType", required = true)
            protected String invType;
            @XmlElement(name = "InvTypeDescription", required = true)
            protected String invTypeDescription;
            @XmlElement(name = "PayInfo")
            protected ProcessResponse.ArrBRM.ArrPayType.PayInfo payInfo;

            /**
             * Gets the value of the invType property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getInvType() {
                return invType;
            }

            /**
             * Sets the value of the invType property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setInvType(String value) {
                this.invType = value;
            }

            /**
             * Gets the value of the invTypeDescription property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getInvTypeDescription() {
                return invTypeDescription;
            }

            /**
             * Sets the value of the invTypeDescription property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setInvTypeDescription(String value) {
                this.invTypeDescription = value;
            }

            /**
             * Gets the value of the payInfo property.
             *
             * @return
             *     possible object is
             *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo }
             *
             */
            public ProcessResponse.ArrBRM.ArrPayType.PayInfo getPayInfo() {
                return payInfo;
            }

            /**
             * Sets the value of the payInfo property.
             *
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo }
             *
             */
            public void setPayInfo(ProcessResponse.ArrBRM.ArrPayType.PayInfo value) {
                this.payInfo = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ArrInv" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="ArrDD" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="ArrCC" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = { "arrInv", "arrDD", "arrCC" })
            public static class PayInfo {

                @XmlElement(name = "ArrInv")
                protected ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv arrInv;
                @XmlElement(name = "ArrDD")
                protected ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD arrDD;
                @XmlElement(name = "ArrCC")
                protected ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC arrCC;

                /**
                 * Gets the value of the arrInv property.
                 *
                 * @return
                 *     possible object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv }
                 *
                 */
                public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv getArrInv() {
                    return arrInv;
                }

                /**
                 * Sets the value of the arrInv property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv }
                 *
                 */
                public void setArrInv(ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrInv value) {
                    this.arrInv = value;
                }

                /**
                 * Gets the value of the arrDD property.
                 *
                 * @return
                 *     possible object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD }
                 *
                 */
                public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD getArrDD() {
                    return arrDD;
                }

                /**
                 * Sets the value of the arrDD property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD }
                 *
                 */
                public void setArrDD(ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrDD value) {
                    this.arrDD = value;
                }

                /**
                 * Gets the value of the arrCC property.
                 *
                 * @return
                 *     possible object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC }
                 *
                 */
                public ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC getArrCC() {
                    return arrCC;
                }

                /**
                 * Sets the value of the arrCC property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC }
                 *
                 */
                public void setArrCC(ProcessResponse.ArrBRM.ArrPayType.PayInfo.ArrCC value) {
                    this.arrCC = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="DebitExp" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "",
                         propOrder = { "address", "city", "country", "debitExp", "debitNum", "name", "state", "zip" })
                public static class ArrCC {

                    @XmlElement(name = "Address", required = true)
                    protected String address;
                    @XmlElement(name = "City", required = true)
                    protected String city;
                    @XmlElement(name = "Country", required = true)
                    protected String country;
                    @XmlElement(name = "DebitExp", required = true)
                    protected String debitExp;
                    @XmlElement(name = "DebitNum", required = true)
                    protected String debitNum;
                    @XmlElement(name = "Name", required = true)
                    protected String name;
                    @XmlElement(name = "State", required = true)
                    protected String state;
                    @XmlElement(name = "Zip", required = true)
                    protected String zip;

                    /**
                     * Gets the value of the address property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getAddress() {
                        return address;
                    }

                    /**
                     * Sets the value of the address property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setAddress(String value) {
                        this.address = value;
                    }

                    /**
                     * Gets the value of the city property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Sets the value of the city property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Gets the value of the country property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCountry() {
                        return country;
                    }

                    /**
                     * Sets the value of the country property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCountry(String value) {
                        this.country = value;
                    }

                    /**
                     * Gets the value of the debitExp property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getDebitExp() {
                        return debitExp;
                    }

                    /**
                     * Sets the value of the debitExp property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setDebitExp(String value) {
                        this.debitExp = value;
                    }

                    /**
                     * Gets the value of the debitNum property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getDebitNum() {
                        return debitNum;
                    }

                    /**
                     * Sets the value of the debitNum property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setDebitNum(String value) {
                        this.debitNum = value;
                    }

                    /**
                     * Gets the value of the name property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * Sets the value of the name property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                    /**
                     * Gets the value of the state property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getState() {
                        return state;
                    }

                    /**
                     * Sets the value of the state property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setState(String value) {
                        this.state = value;
                    }

                    /**
                     * Gets the value of the zip property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getZip() {
                        return zip;
                    }

                    /**
                     * Sets the value of the zip property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setZip(String value) {
                        this.zip = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Bankno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="DebitNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "",
                         propOrder =
                         { "address", "bankno", "city", "country", "debitNum", "name", "state", "type", "zip"
                    })
                public static class ArrDD {

                    @XmlElement(name = "Address", required = true)
                    protected String address;
                    @XmlElement(name = "Bankno", required = true)
                    protected String bankno;
                    @XmlElement(name = "City", required = true)
                    protected String city;
                    @XmlElement(name = "Country", required = true)
                    protected String country;
                    @XmlElement(name = "DebitNum", required = true)
                    protected String debitNum;
                    @XmlElement(name = "Name", required = true)
                    protected String name;
                    @XmlElement(name = "State", required = true)
                    protected String state;
                    @XmlElement(name = "Type", required = true)
                    protected String type;
                    @XmlElement(name = "Zip", required = true)
                    protected String zip;

                    /**
                     * Gets the value of the address property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getAddress() {
                        return address;
                    }

                    /**
                     * Sets the value of the address property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setAddress(String value) {
                        this.address = value;
                    }

                    /**
                     * Gets the value of the bankno property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getBankno() {
                        return bankno;
                    }

                    /**
                     * Sets the value of the bankno property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setBankno(String value) {
                        this.bankno = value;
                    }

                    /**
                     * Gets the value of the city property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Sets the value of the city property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Gets the value of the country property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCountry() {
                        return country;
                    }

                    /**
                     * Sets the value of the country property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCountry(String value) {
                        this.country = value;
                    }

                    /**
                     * Gets the value of the debitNum property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getDebitNum() {
                        return debitNum;
                    }

                    /**
                     * Sets the value of the debitNum property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setDebitNum(String value) {
                        this.debitNum = value;
                    }

                    /**
                     * Gets the value of the name property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * Sets the value of the name property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                    /**
                     * Gets the value of the state property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getState() {
                        return state;
                    }

                    /**
                     * Sets the value of the state property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setState(String value) {
                        this.state = value;
                    }

                    /**
                     * Gets the value of the type property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                    /**
                     * Gets the value of the zip property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getZip() {
                        return zip;
                    }

                    /**
                     * Sets the value of the zip property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setZip(String value) {
                        this.zip = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="DeliveryPrefer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="InvInstr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="EmailAddr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "",
                         propOrder =
                         { "address", "city", "country", "deliveryPrefer", "invInstr", "emailAddr", "name", "state",
                           "zip"
                    })
                public static class ArrInv {

                    @XmlElement(name = "Address", required = true)
                    protected String address;
                    @XmlElement(name = "City", required = true)
                    protected String city;
                    @XmlElement(name = "Country", required = true)
                    protected String country;
                    @XmlElement(name = "DeliveryPrefer", required = true)
                    protected String deliveryPrefer;
                    @XmlElement(name = "InvInstr", required = true)
                    protected String invInstr;
                    @XmlElement(name = "EmailAddr", required = true)
                    protected String emailAddr;
                    @XmlElement(name = "Name", required = true)
                    protected String name;
                    @XmlElement(name = "State", required = true)
                    protected String state;
                    @XmlElement(name = "Zip", required = true)
                    protected String zip;

                    /**
                     * Gets the value of the address property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getAddress() {
                        return address;
                    }

                    /**
                     * Sets the value of the address property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setAddress(String value) {
                        this.address = value;
                    }

                    /**
                     * Gets the value of the city property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Sets the value of the city property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Gets the value of the country property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCountry() {
                        return country;
                    }

                    /**
                     * Sets the value of the country property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCountry(String value) {
                        this.country = value;
                    }

                    /**
                     * Gets the value of the deliveryPrefer property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getDeliveryPrefer() {
                        return deliveryPrefer;
                    }

                    /**
                     * Sets the value of the deliveryPrefer property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setDeliveryPrefer(String value) {
                        this.deliveryPrefer = value;
                    }

                    /**
                     * Gets the value of the invInstr property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getInvInstr() {
                        return invInstr;
                    }

                    /**
                     * Sets the value of the invInstr property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setInvInstr(String value) {
                        this.invInstr = value;
                    }

                    /**
                     * Gets the value of the emailAddr property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getEmailAddr() {
                        return emailAddr;
                    }

                    /**
                     * Sets the value of the emailAddr property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setEmailAddr(String value) {
                        this.emailAddr = value;
                    }

                    /**
                     * Gets the value of the name property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * Sets the value of the name property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                    /**
                     * Gets the value of the state property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getState() {
                        return state;
                    }

                    /**
                     * Sets the value of the state property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setState(String value) {
                        this.state = value;
                    }

                    /**
                     * Gets the value of the zip property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getZip() {
                        return zip;
                    }

                    /**
                     * Sets the value of the zip property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setZip(String value) {
                        this.zip = value;
                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Servicio" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
         *                   &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="ArrProduct"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                                       &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                                       &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                                       &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                                       &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="ArrDiscount"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                                       &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *                                       &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                       &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "servicio" })
        public static class ArrServices {

            @XmlElement(name = "Servicio")
            protected List<ProcessResponse.ArrBRM.ArrServices.Servicio> servicio;

            /**
             * Gets the value of the servicio property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the servicio property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getServicio().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProcessResponse.ArrBRM.ArrServices.Servicio }
             *
             *
             */
            public List<ProcessResponse.ArrBRM.ArrServices.Servicio> getServicio() {
                if (servicio == null) {
                    servicio = new ArrayList<ProcessResponse.ArrBRM.ArrServices.Servicio>();
                }
                return this.servicio;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="DN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Incluido_Flag" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
             *         &lt;element name="PackageDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Latitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Longuitud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="ArrProduct"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *                             &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *                             &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *                             &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *                             &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="ArrDiscount"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *                             &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
             *                             &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                             &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder =
                     { "service", "serialNumber", "mac", "dn", "incluidoFlag", "packageDescription", "packageId",
                       "latitud", "longuitud", "creditLimit", "arrProduct", "arrDiscount"
                })
            public static class Servicio {

                @XmlElement(name = "Service", required = true)
                protected String service;
                @XmlElement(name = "SerialNumber", required = true)
                protected String serialNumber;
                @XmlElement(name = "Mac", required = true)
                protected String mac;
                @XmlElement(name = "DN", required = true)
                protected String dn;
                @XmlElement(name = "Incluido_Flag")
                protected int incluidoFlag;
                @XmlElement(name = "PackageDescription", required = true)
                protected String packageDescription;
                @XmlElement(name = "PackageId", required = true)
                protected String packageId;
                @XmlElement(name = "Latitud", required = true)
                protected String latitud;
                @XmlElement(name = "Longuitud", required = true)
                protected String longuitud;
                @XmlElement(name = "CreditLimit", required = true)
                protected String creditLimit;
                @XmlElement(name = "ArrProduct", required = true)
                protected ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct arrProduct;
                @XmlElement(name = "ArrDiscount", required = true)
                protected ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount arrDiscount;

                /**
                 * Gets the value of the service property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getService() {
                    return service;
                }

                /**
                 * Sets the value of the service property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setService(String value) {
                    this.service = value;
                }

                /**
                 * Gets the value of the serialNumber property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSerialNumber() {
                    return serialNumber;
                }

                /**
                 * Sets the value of the serialNumber property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSerialNumber(String value) {
                    this.serialNumber = value;
                }

                /**
                 * Gets the value of the mac property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getMac() {
                    return mac;
                }

                /**
                 * Sets the value of the mac property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setMac(String value) {
                    this.mac = value;
                }

                /**
                 * Gets the value of the dn property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getDN() {
                    return dn;
                }

                /**
                 * Sets the value of the dn property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setDN(String value) {
                    this.dn = value;
                }

                /**
                 * Gets the value of the incluidoFlag property.
                 *
                 */
                public int getIncluidoFlag() {
                    return incluidoFlag;
                }

                /**
                 * Sets the value of the incluidoFlag property.
                 *
                 */
                public void setIncluidoFlag(int value) {
                    this.incluidoFlag = value;
                }

                /**
                 * Gets the value of the packageDescription property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getPackageDescription() {
                    return packageDescription;
                }

                /**
                 * Sets the value of the packageDescription property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setPackageDescription(String value) {
                    this.packageDescription = value;
                }

                /**
                 * Gets the value of the packageId property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getPackageId() {
                    return packageId;
                }

                /**
                 * Sets the value of the packageId property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setPackageId(String value) {
                    this.packageId = value;
                }

                /**
                 * Gets the value of the latitud property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getLatitud() {
                    return latitud;
                }

                /**
                 * Sets the value of the latitud property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setLatitud(String value) {
                    this.latitud = value;
                }

                /**
                 * Gets the value of the longuitud property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getLonguitud() {
                    return longuitud;
                }

                /**
                 * Sets the value of the longuitud property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setLonguitud(String value) {
                    this.longuitud = value;
                }

                /**
                 * Gets the value of the creditLimit property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCreditLimit() {
                    return creditLimit;
                }

                /**
                 * Sets the value of the creditLimit property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCreditLimit(String value) {
                    this.creditLimit = value;
                }

                /**
                 * Gets the value of the arrProduct property.
                 *
                 * @return
                 *     possible object is
                 *     {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct }
                 *
                 */
                public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct getArrProduct() {
                    return arrProduct;
                }

                /**
                 * Sets the value of the arrProduct property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct }
                 *
                 */
                public void setArrProduct(ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct value) {
                    this.arrProduct = value;
                }

                /**
                 * Gets the value of the arrDiscount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount }
                 *
                 */
                public ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount getArrDiscount() {
                    return arrDiscount;
                }

                /**
                 * Sets the value of the arrDiscount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount }
                 *
                 */
                public void setArrDiscount(ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount value) {
                    this.arrDiscount = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                 *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = { "discount" })
                public static class ArrDiscount {

                    @XmlElement(name = "Discount")
                    protected List<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount> discount;

                    /**
                     * Gets the value of the discount property.
                     *
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the discount property.
                     *
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getDiscount().add(newItem);
                     * </pre>
                     *
                     *
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount }
                     *
                     *
                     */
                    public List<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount> getDiscount() {
                        if (discount == null) {
                            discount =
                                new ArrayList<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrDiscount.Discount>();
                        }
                        return this.discount;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     *
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     *
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                     *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     *
                     *
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "",
                             propOrder =
                             { "idProduct", "startDate", "endDate", "quantity", "status", "additionalDescription",
                               "packageId"
                        })
                    public static class Discount {

                        @XmlElement(name = "IdProduct", required = true)
                        protected String idProduct;
                        @XmlElement(name = "StartDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar startDate;
                        @XmlElement(name = "EndDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar endDate;
                        @XmlElement(name = "Quantity", required = true)
                        protected BigDecimal quantity;
                        @XmlElement(name = "Status", required = true)
                        protected String status;
                        @XmlElement(name = "AdditionalDescription", required = true)
                        protected String additionalDescription;
                        @XmlElement(name = "PackageId", required = true)
                        protected String packageId;

                        /**
                         * Gets the value of the idProduct property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getIdProduct() {
                            return idProduct;
                        }

                        /**
                         * Sets the value of the idProduct property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setIdProduct(String value) {
                            this.idProduct = value;
                        }

                        /**
                         * Gets the value of the startDate property.
                         *
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public XMLGregorianCalendar getStartDate() {
                            return startDate;
                        }

                        /**
                         * Sets the value of the startDate property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public void setStartDate(XMLGregorianCalendar value) {
                            this.startDate = value;
                        }

                        /**
                         * Gets the value of the endDate property.
                         *
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public XMLGregorianCalendar getEndDate() {
                            return endDate;
                        }

                        /**
                         * Sets the value of the endDate property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public void setEndDate(XMLGregorianCalendar value) {
                            this.endDate = value;
                        }

                        /**
                         * Gets the value of the quantity property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *
                         */
                        public BigDecimal getQuantity() {
                            return quantity;
                        }

                        /**
                         * Sets the value of the quantity property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *
                         */
                        public void setQuantity(BigDecimal value) {
                            this.quantity = value;
                        }

                        /**
                         * Gets the value of the status property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getStatus() {
                            return status;
                        }

                        /**
                         * Sets the value of the status property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setStatus(String value) {
                            this.status = value;
                        }

                        /**
                         * Gets the value of the additionalDescription property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getAdditionalDescription() {
                            return additionalDescription;
                        }

                        /**
                         * Sets the value of the additionalDescription property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setAdditionalDescription(String value) {
                            this.additionalDescription = value;
                        }

                        /**
                         * Gets the value of the packageId property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getPackageId() {
                            return packageId;
                        }

                        /**
                         * Sets the value of the packageId property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setPackageId(String value) {
                            this.packageId = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Product" maxOccurs="unbounded" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                 *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                 *                   &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                 *                   &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                   &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = { "product" })
                public static class ArrProduct {

                    @XmlElement(name = "Product")
                    protected List<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product> product;

                    /**
                     * Gets the value of the product property.
                     *
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the product property.
                     *
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getProduct().add(newItem);
                     * </pre>
                     *
                     *
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product }
                     *
                     *
                     */
                    public List<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product> getProduct() {
                        if (product == null) {
                            product = new ArrayList<ProcessResponse.ArrBRM.ArrServices.Servicio.ArrProduct.Product>();
                        }
                        return this.product;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     *
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     *
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="IdProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                     *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
                     *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                     *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
                     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="AdditionalDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *         &lt;element name="IncludeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     *
                     *
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "",
                             propOrder =
                             { "idProduct", "startDate", "endDate", "price", "discount", "quantity", "status",
                               "statusDescription", "additionalDescription", "packageId", "includeFlag"
                        })
                    public static class Product {

                        @XmlElement(name = "IdProduct", required = true)
                        protected String idProduct;
                        @XmlElement(name = "StartDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar startDate;
                        @XmlElement(name = "EndDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar endDate;
                        @XmlElement(name = "Price", required = true)
                        protected BigDecimal price;
                        @XmlElement(name = "Discount", required = true)
                        protected BigDecimal discount;
                        @XmlElement(name = "Quantity", required = true)
                        protected BigDecimal quantity;
                        @XmlElement(name = "Status", required = true)
                        protected String status;
                        @XmlElement(name = "StatusDescription", required = true)
                        protected String statusDescription;
                        @XmlElement(name = "AdditionalDescription", required = true)
                        protected String additionalDescription;
                        @XmlElement(name = "PackageId", required = true)
                        protected String packageId;
                        @XmlElement(name = "IncludeFlag", required = true)
                        protected String includeFlag;

                        /**
                         * Gets the value of the idProduct property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getIdProduct() {
                            return idProduct;
                        }

                        /**
                         * Sets the value of the idProduct property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setIdProduct(String value) {
                            this.idProduct = value;
                        }

                        /**
                         * Gets the value of the startDate property.
                         *
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public XMLGregorianCalendar getStartDate() {
                            return startDate;
                        }

                        /**
                         * Sets the value of the startDate property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public void setStartDate(XMLGregorianCalendar value) {
                            this.startDate = value;
                        }

                        /**
                         * Gets the value of the endDate property.
                         *
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public XMLGregorianCalendar getEndDate() {
                            return endDate;
                        }

                        /**
                         * Sets the value of the endDate property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *
                         */
                        public void setEndDate(XMLGregorianCalendar value) {
                            this.endDate = value;
                        }

                        /**
                         * Gets the value of the price property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *
                         */
                        public BigDecimal getPrice() {
                            return price;
                        }

                        /**
                         * Sets the value of the price property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *
                         */
                        public void setPrice(BigDecimal value) {
                            this.price = value;
                        }

                        /**
                         * Gets the value of the discount property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *
                         */
                        public BigDecimal getDiscount() {
                            return discount;
                        }

                        /**
                         * Sets the value of the discount property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *
                         */
                        public void setDiscount(BigDecimal value) {
                            this.discount = value;
                        }

                        /**
                         * Gets the value of the quantity property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *
                         */
                        public BigDecimal getQuantity() {
                            return quantity;
                        }

                        /**
                         * Sets the value of the quantity property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *
                         */
                        public void setQuantity(BigDecimal value) {
                            this.quantity = value;
                        }

                        /**
                         * Gets the value of the status property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getStatus() {
                            return status;
                        }

                        /**
                         * Sets the value of the status property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setStatus(String value) {
                            this.status = value;
                        }

                        /**
                         * Gets the value of the statusDescription property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getStatusDescription() {
                            return statusDescription;
                        }

                        /**
                         * Sets the value of the statusDescription property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setStatusDescription(String value) {
                            this.statusDescription = value;
                        }

                        /**
                         * Gets the value of the additionalDescription property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getAdditionalDescription() {
                            return additionalDescription;
                        }

                        /**
                         * Sets the value of the additionalDescription property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setAdditionalDescription(String value) {
                            this.additionalDescription = value;
                        }

                        /**
                         * Gets the value of the packageId property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getPackageId() {
                            return packageId;
                        }

                        /**
                         * Sets the value of the packageId property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setPackageId(String value) {
                            this.packageId = value;
                        }

                        /**
                         * Gets the value of the includeFlag property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getIncludeFlag() {
                            return includeFlag;
                        }

                        /**
                         * Sets the value of the includeFlag property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setIncludeFlag(String value) {
                            this.includeFlag = value;
                        }

                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="LastBillBalance"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="SummaryStatement"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="LastInvoiceSummary"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="historicoFacturacion"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                             &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "",
                 propOrder = { "lastBillBalance", "summaryStatement", "lastInvoiceSummary", "historicoFacturacion" })
        public static class BillInfo {

            @XmlElement(name = "LastBillBalance", required = true)
            protected ProcessResponse.ArrBRM.BillInfo.LastBillBalance lastBillBalance;
            @XmlElement(name = "SummaryStatement", required = true)
            protected ProcessResponse.ArrBRM.BillInfo.SummaryStatement summaryStatement;
            @XmlElement(name = "LastInvoiceSummary", required = true)
            protected ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary lastInvoiceSummary;
            @XmlElement(required = true)
            protected ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion historicoFacturacion;

            /**
             * Gets the value of the lastBillBalance property.
             *
             * @return
             *     possible object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.LastBillBalance }
             *
             */
            public ProcessResponse.ArrBRM.BillInfo.LastBillBalance getLastBillBalance() {
                return lastBillBalance;
            }

            /**
             * Sets the value of the lastBillBalance property.
             *
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.LastBillBalance }
             *
             */
            public void setLastBillBalance(ProcessResponse.ArrBRM.BillInfo.LastBillBalance value) {
                this.lastBillBalance = value;
            }

            /**
             * Gets the value of the summaryStatement property.
             *
             * @return
             *     possible object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.SummaryStatement }
             *
             */
            public ProcessResponse.ArrBRM.BillInfo.SummaryStatement getSummaryStatement() {
                return summaryStatement;
            }

            /**
             * Sets the value of the summaryStatement property.
             *
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.SummaryStatement }
             *
             */
            public void setSummaryStatement(ProcessResponse.ArrBRM.BillInfo.SummaryStatement value) {
                this.summaryStatement = value;
            }

            /**
             * Gets the value of the lastInvoiceSummary property.
             *
             * @return
             *     possible object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary }
             *
             */
            public ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary getLastInvoiceSummary() {
                return lastInvoiceSummary;
            }

            /**
             * Sets the value of the lastInvoiceSummary property.
             *
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary }
             *
             */
            public void setLastInvoiceSummary(ProcessResponse.ArrBRM.BillInfo.LastInvoiceSummary value) {
                this.lastInvoiceSummary = value;
            }

            /**
             * Gets the value of the historicoFacturacion property.
             *
             * @return
             *     possible object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion }
             *
             */
            public ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion getHistoricoFacturacion() {
                return historicoFacturacion;
            }

            /**
             * Sets the value of the historicoFacturacion property.
             *
             * @param value
             *     allowed object is
             *     {@link ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion }
             *
             */
            public void setHistoricoFacturacion(ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion value) {
                this.historicoFacturacion = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Factura" maxOccurs="3" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = { "factura" })
            public static class HistoricoFacturacion {

                @XmlElement(name = "Factura")
                protected List<ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura> factura;

                /**
                 * Gets the value of the factura property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the factura property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getFactura().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura }
                 *
                 *
                 */
                public List<ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura> getFactura() {
                    if (factura == null) {
                        factura = new ArrayList<ProcessResponse.ArrBRM.BillInfo.HistoricoFacturacion.Factura>();
                    }
                    return this.factura;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 *
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="bilingDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = { "bilingDate", "amount" })
                public static class Factura {

                    @XmlElement(required = true)
                    protected String bilingDate;
                    @XmlElement(required = true)
                    protected String amount;

                    /**
                     * Gets the value of the bilingDate property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getBilingDate() {
                        return bilingDate;
                    }

                    /**
                     * Sets the value of the bilingDate property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setBilingDate(String value) {
                        this.bilingDate = value;
                    }

                    /**
                     * Gets the value of the amount property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getAmount() {
                        return amount;
                    }

                    /**
                     * Sets the value of the amount property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setAmount(String value) {
                        this.amount = value;
                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="endT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="totalPaydiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="endTDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="courtT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder = { "totalPay", "currentBilling", "endT", "totalPaydiscount", "endTDiscount", "courtT" })
            public static class LastBillBalance {

                @XmlElement(required = true)
                protected BigDecimal totalPay;
                @XmlElement(required = true)
                protected BigDecimal currentBilling;
                @XmlElement(required = true)
                protected String endT;
                @XmlElement(required = true)
                protected BigDecimal totalPaydiscount;
                @XmlElement(required = true)
                protected String endTDiscount;
                @XmlElement(required = true)
                protected String courtT;

                /**
                 * Gets the value of the totalPay property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getTotalPay() {
                    return totalPay;
                }

                /**
                 * Sets the value of the totalPay property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setTotalPay(BigDecimal value) {
                    this.totalPay = value;
                }

                /**
                 * Gets the value of the currentBilling property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getCurrentBilling() {
                    return currentBilling;
                }

                /**
                 * Sets the value of the currentBilling property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setCurrentBilling(BigDecimal value) {
                    this.currentBilling = value;
                }

                /**
                 * Gets the value of the endT property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEndT() {
                    return endT;
                }

                /**
                 * Sets the value of the endT property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEndT(String value) {
                    this.endT = value;
                }

                /**
                 * Gets the value of the totalPaydiscount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getTotalPaydiscount() {
                    return totalPaydiscount;
                }

                /**
                 * Sets the value of the totalPaydiscount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setTotalPaydiscount(BigDecimal value) {
                    this.totalPaydiscount = value;
                }

                /**
                 * Gets the value of the endTDiscount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEndTDiscount() {
                    return endTDiscount;
                }

                /**
                 * Sets the value of the endTDiscount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEndTDiscount(String value) {
                    this.endTDiscount = value;
                }

                /**
                 * Gets the value of the courtT property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCourtT() {
                    return courtT;
                }

                /**
                 * Sets the value of the courtT property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCourtT(String value) {
                    this.courtT = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="bonosesPromotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="periodConsum" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="totalBills" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "",
                     propOrder = { "period", "rent", "bonosesPromotion", "periodConsum", "subtotal", "totalBills" })
            public static class LastInvoiceSummary {

                @XmlElement(required = true)
                protected String period;
                @XmlElement(required = true)
                protected BigDecimal rent;
                @XmlElement(required = true)
                protected BigDecimal bonosesPromotion;
                @XmlElement(required = true)
                protected BigDecimal periodConsum;
                @XmlElement(required = true)
                protected BigDecimal subtotal;
                @XmlElement(required = true)
                protected BigDecimal totalBills;

                /**
                 * Gets the value of the period property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getPeriod() {
                    return period;
                }

                /**
                 * Sets the value of the period property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setPeriod(String value) {
                    this.period = value;
                }

                /**
                 * Gets the value of the rent property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getRent() {
                    return rent;
                }

                /**
                 * Sets the value of the rent property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setRent(BigDecimal value) {
                    this.rent = value;
                }

                /**
                 * Gets the value of the bonosesPromotion property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getBonosesPromotion() {
                    return bonosesPromotion;
                }

                /**
                 * Sets the value of the bonosesPromotion property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setBonosesPromotion(BigDecimal value) {
                    this.bonosesPromotion = value;
                }

                /**
                 * Gets the value of the periodConsum property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getPeriodConsum() {
                    return periodConsum;
                }

                /**
                 * Sets the value of the periodConsum property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setPeriodConsum(BigDecimal value) {
                    this.periodConsum = value;
                }

                /**
                 * Gets the value of the subtotal property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getSubtotal() {
                    return subtotal;
                }

                /**
                 * Sets the value of the subtotal property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setSubtotal(BigDecimal value) {
                    this.subtotal = value;
                }

                /**
                 * Gets the value of the totalBills property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getTotalBills() {
                    return totalBills;
                }

                /**
                 * Sets the value of the totalBills property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setTotalBills(BigDecimal value) {
                    this.totalBills = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="previiousBill" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="payment" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="creditNote" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="currentBilling" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="totalPay" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = { "previiousBill", "payment", "creditNote", "currentBilling", "totalPay" })
            public static class SummaryStatement {

                @XmlElement(required = true)
                protected BigDecimal previiousBill;
                @XmlElement(required = true)
                protected BigDecimal payment;
                @XmlElement(required = true)
                protected BigDecimal creditNote;
                @XmlElement(required = true)
                protected BigDecimal currentBilling;
                @XmlElement(required = true)
                protected BigDecimal totalPay;

                /**
                 * Gets the value of the previiousBill property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getPreviiousBill() {
                    return previiousBill;
                }

                /**
                 * Sets the value of the previiousBill property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setPreviiousBill(BigDecimal value) {
                    this.previiousBill = value;
                }

                /**
                 * Gets the value of the payment property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getPayment() {
                    return payment;
                }

                /**
                 * Sets the value of the payment property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setPayment(BigDecimal value) {
                    this.payment = value;
                }

                /**
                 * Gets the value of the creditNote property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getCreditNote() {
                    return creditNote;
                }

                /**
                 * Sets the value of the creditNote property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setCreditNote(BigDecimal value) {
                    this.creditNote = value;
                }

                /**
                 * Gets the value of the currentBilling property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getCurrentBilling() {
                    return currentBilling;
                }

                /**
                 * Sets the value of the currentBilling property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setCurrentBilling(BigDecimal value) {
                    this.currentBilling = value;
                }

                /**
                 * Gets the value of the totalPay property.
                 *
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *
                 */
                public BigDecimal getTotalPay() {
                    return totalPay;
                }

                /**
                 * Sets the value of the totalPay property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *
                 */
                public void setTotalPay(BigDecimal value) {
                    this.totalPay = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "startDate", "endDate" })
        public static class Contract {

            @XmlElement(name = "StartDate", required = true)
            protected String startDate;
            @XmlElement(name = "EndDate", required = true)
            protected String endDate;

            /**
             * Gets the value of the startDate property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStartDate() {
                return startDate;
            }

            /**
             * Sets the value of the startDate property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStartDate(String value) {
                this.startDate = value;
            }

            /**
             * Gets the value of the endDate property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getEndDate() {
                return endDate;
            }

            /**
             * Sets the value of the endDate property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setEndDate(String value) {
                this.endDate = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Payment" maxOccurs="3"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = { "payment" })
        public static class Last3Payments {

            @XmlElement(name = "Payment", required = true)
            protected List<ProcessResponse.ArrBRM.Last3Payments.Payment> payment;

            /**
             * Gets the value of the payment property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the payment property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPayment().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProcessResponse.ArrBRM.Last3Payments.Payment }
             *
             *
             */
            public List<ProcessResponse.ArrBRM.Last3Payments.Payment> getPayment() {
                if (payment == null) {
                    payment = new ArrayList<ProcessResponse.ArrBRM.Last3Payments.Payment>();
                }
                return this.payment;
            }


            /**
             * <p>Java class for anonymous complex type.
             *
             * <p>The following schema fragment specifies the expected content contained within this class.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Effective_T" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = { "effectiveT", "amount" })
            public static class Payment {

                @XmlElement(name = "Effective_T", required = true)
                protected String effectiveT;
                @XmlElement(name = "Amount", required = true)
                protected String amount;

                /**
                 * Gets the value of the effectiveT property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEffectiveT() {
                    return effectiveT;
                }

                /**
                 * Sets the value of the effectiveT property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEffectiveT(String value) {
                    this.effectiveT = value;
                }

                /**
                 * Gets the value of the amount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setAmount(String value) {
                    this.amount = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="BANCO_AZTECA" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="BANCOMER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="HSBC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="BANAMEX" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="BANORTE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="SCOTIABANK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="SANTANDER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "",
                 propOrder = { "bancoazteca", "bancomer", "hsbc", "banamex", "banorte", "scotiabank", "santander" })
        public static class ReferenciasBancarias {

            @XmlElement(name = "BANCO_AZTECA", required = true)
            protected String bancoazteca;
            @XmlElement(name = "BANCOMER", required = true)
            protected String bancomer;
            @XmlElement(name = "HSBC", required = true)
            protected String hsbc;
            @XmlElement(name = "BANAMEX", required = true)
            protected String banamex;
            @XmlElement(name = "BANORTE", required = true)
            protected String banorte;
            @XmlElement(name = "SCOTIABANK", required = true)
            protected String scotiabank;
            @XmlElement(name = "SANTANDER", required = true)
            protected String santander;

            /**
             * Gets the value of the bancoazteca property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBANCOAZTECA() {
                return bancoazteca;
            }

            /**
             * Sets the value of the bancoazteca property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBANCOAZTECA(String value) {
                this.bancoazteca = value;
            }

            /**
             * Gets the value of the bancomer property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBANCOMER() {
                return bancomer;
            }

            /**
             * Sets the value of the bancomer property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBANCOMER(String value) {
                this.bancomer = value;
            }

            /**
             * Gets the value of the hsbc property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getHSBC() {
                return hsbc;
            }

            /**
             * Sets the value of the hsbc property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setHSBC(String value) {
                this.hsbc = value;
            }

            /**
             * Gets the value of the banamex property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBANAMEX() {
                return banamex;
            }

            /**
             * Sets the value of the banamex property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBANAMEX(String value) {
                this.banamex = value;
            }

            /**
             * Gets the value of the banorte property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getBANORTE() {
                return banorte;
            }

            /**
             * Sets the value of the banorte property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setBANORTE(String value) {
                this.banorte = value;
            }

            /**
             * Gets the value of the scotiabank property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getSCOTIABANK() {
                return scotiabank;
            }

            /**
             * Sets the value of the scotiabank property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setSCOTIABANK(String value) {
                this.scotiabank = value;
            }

            /**
             * Gets the value of the santander property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getSANTANDER() {
                return santander;
            }

            /**
             * Sets the value of the santander property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setSANTANDER(String value) {
                this.santander = value;
            }

        }

    }

}
