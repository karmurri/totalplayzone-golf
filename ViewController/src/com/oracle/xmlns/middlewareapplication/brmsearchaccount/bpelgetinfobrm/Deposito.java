
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="depositoG" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Deposito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "depositoG" })
@XmlRootElement(name = "deposito")
public class Deposito {

    @XmlElement(required = true)
    protected List<Deposito.DepositoG> depositoG;

    /**
     * Gets the value of the depositoG property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the depositoG property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepositoG().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Deposito.DepositoG }
     *
     *
     */
    public List<Deposito.DepositoG> getDepositoG() {
        if (depositoG == null) {
            depositoG = new ArrayList<Deposito.DepositoG>();
        }
        return this.depositoG;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Deposito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "deposito", "cantidad" })
    public static class DepositoG {

        @XmlElement(name = "Deposito", required = true)
        protected String deposito;
        protected int cantidad;

        /**
         * Gets the value of the deposito property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDeposito() {
            return deposito;
        }

        /**
         * Sets the value of the deposito property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDeposito(String value) {
            this.deposito = value;
        }

        /**
         * Gets the value of the cantidad property.
         *
         */
        public int getCantidad() {
            return cantidad;
        }

        /**
         * Sets the value of the cantidad property.
         *
         */
        public void setCantidad(int value) {
            this.cantidad = value;
        }

    }

}
