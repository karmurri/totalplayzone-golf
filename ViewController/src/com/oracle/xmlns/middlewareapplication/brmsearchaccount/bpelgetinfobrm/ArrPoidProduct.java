
package com.oracle.xmlns.middlewareapplication.brmsearchaccount.bpelgetinfobrm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PoidProduct" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="tcgId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="tcgParameter" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="tcgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "poidProduct" })
@XmlRootElement(name = "arrPoidProduct")
public class ArrPoidProduct {

    @XmlElement(name = "PoidProduct", required = true)
    protected List<ArrPoidProduct.PoidProduct> poidProduct;

    /**
     * Gets the value of the poidProduct property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the poidProduct property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPoidProduct().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrPoidProduct.PoidProduct }
     *
     *
     */
    public List<ArrPoidProduct.PoidProduct> getPoidProduct() {
        if (poidProduct == null) {
            poidProduct = new ArrayList<ArrPoidProduct.PoidProduct>();
        }
        return this.poidProduct;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="tcgId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="tcgParameter" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="tcgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "tcgId", "tcgParameter", "tcgValue" })
    public static class PoidProduct {

        @XmlElement(required = true)
        protected String tcgId;
        @XmlElement(required = true)
        protected String tcgParameter;
        @XmlElement(required = true)
        protected String tcgValue;

        /**
         * Gets the value of the tcgId property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTcgId() {
            return tcgId;
        }

        /**
         * Sets the value of the tcgId property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTcgId(String value) {
            this.tcgId = value;
        }

        /**
         * Gets the value of the tcgParameter property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTcgParameter() {
            return tcgParameter;
        }

        /**
         * Sets the value of the tcgParameter property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTcgParameter(String value) {
            this.tcgParameter = value;
        }

        /**
         * Gets the value of the tcgValue property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTcgValue() {
            return tcgValue;
        }

        /**
         * Sets the value of the tcgValue property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTcgValue(String value) {
            this.tcgValue = value;
        }

    }

}
