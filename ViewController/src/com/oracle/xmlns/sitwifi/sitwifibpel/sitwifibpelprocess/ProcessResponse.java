
package com.oracle.xmlns.sitwifi.sitwifibpel.sitwifibpelprocess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Results"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Response"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="respuestaSitWiFi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "results", "response" })
@XmlRootElement(name = "processResponse")
public class ProcessResponse {

    @XmlElement(name = "Results", required = true)
    protected ProcessResponse.Results results;
    @XmlElement(name = "Response", required = true)
    protected ProcessResponse.Response response;

    /**
     * Gets the value of the results property.
     *
     * @return
     *     possible object is
     *     {@link ProcessResponse.Results }
     *
     */
    public ProcessResponse.Results getResults() {
        return results;
    }

    /**
     * Sets the value of the results property.
     *
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Results }
     *
     */
    public void setResults(ProcessResponse.Results value) {
        this.results = value;
    }

    /**
     * Gets the value of the response property.
     *
     * @return
     *     possible object is
     *     {@link ProcessResponse.Response }
     *
     */
    public ProcessResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     *
     * @param value
     *     allowed object is
     *     {@link ProcessResponse.Response }
     *
     */
    public void setResponse(ProcessResponse.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="respuestaSitWiFi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "respuestaSitWiFi" })
    public static class Response {

        @XmlElement(required = true)
        protected String respuestaSitWiFi;

        /**
         * Gets the value of the respuestaSitWiFi property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getRespuestaSitWiFi() {
            return respuestaSitWiFi;
        }

        /**
         * Sets the value of the respuestaSitWiFi property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setRespuestaSitWiFi(String value) {
            this.respuestaSitWiFi = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="IdResult" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "idResult", "code", "description" })
    public static class Results {

        @XmlElement(name = "IdResult", required = true)
        protected String idResult;
        @XmlElement(name = "Code", required = true)
        protected String code;
        @XmlElement(name = "Description", required = true)
        protected String description;

        /**
         * Gets the value of the idResult property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getIdResult() {
            return idResult;
        }

        /**
         * Sets the value of the idResult property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setIdResult(String value) {
            this.idResult = value;
        }

        /**
         * Gets the value of the code property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
