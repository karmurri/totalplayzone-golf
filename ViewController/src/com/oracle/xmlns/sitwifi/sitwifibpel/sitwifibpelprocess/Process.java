
package com.oracle.xmlns.sitwifi.sitwifibpel.sitwifibpelprocess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Login"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Ip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="InputElements"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="NumeroDeCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Contraseña" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Proxy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ClientMac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "login", "inputElements" })
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(name = "Login", required = true)
    protected Process.Login login;
    @XmlElement(name = "InputElements", required = true)
    protected Process.InputElements inputElements;

    /**
     * Gets the value of the login property.
     *
     * @return
     *     possible object is
     *     {@link Process.Login }
     *
     */
    public Process.Login getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value
     *     allowed object is
     *     {@link Process.Login }
     *
     */
    public void setLogin(Process.Login value) {
        this.login = value;
    }

    /**
     * Gets the value of the inputElements property.
     *
     * @return
     *     possible object is
     *     {@link Process.InputElements }
     *
     */
    public Process.InputElements getInputElements() {
        return inputElements;
    }

    /**
     * Sets the value of the inputElements property.
     *
     * @param value
     *     allowed object is
     *     {@link Process.InputElements }
     *
     */
    public void setInputElements(Process.InputElements value) {
        this.inputElements = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="NumeroDeCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Contraseña" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Proxy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ClientMac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "numeroDeCliente", "contrase\u00f1a", "url", "proxy", "uip", "clientMac" })
    public static class InputElements {

        @XmlElement(name = "NumeroDeCliente")
        protected String numeroDeCliente;
        @XmlElement(name = "Contrase\u00f1a")
        protected String contraseña;
        @XmlElement(name = "URL")
        protected String url;
        @XmlElement(name = "Proxy")
        protected String proxy;
        @XmlElement(name = "UIP")
        protected String uip;
        @XmlElement(name = "ClientMac")
        protected String clientMac;

        /**
         * Gets the value of the numeroDeCliente property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getNumeroDeCliente() {
            return numeroDeCliente;
        }

        /**
         * Sets the value of the numeroDeCliente property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setNumeroDeCliente(String value) {
            this.numeroDeCliente = value;
        }

        /**
         * Gets the value of the contraseña property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getContraseña() {
            return contraseña;
        }

        /**
         * Sets the value of the contraseña property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setContraseña(String value) {
            this.contraseña = value;
        }

        /**
         * Gets the value of the url property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getURL() {
            return url;
        }

        /**
         * Sets the value of the url property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setURL(String value) {
            this.url = value;
        }

        /**
         * Gets the value of the proxy property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getProxy() {
            return proxy;
        }

        /**
         * Sets the value of the proxy property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setProxy(String value) {
            this.proxy = value;
        }

        /**
         * Gets the value of the uip property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUIP() {
            return uip;
        }

        /**
         * Sets the value of the uip property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUIP(String value) {
            this.uip = value;
        }

        /**
         * Gets the value of the clientMac property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getClientMac() {
            return clientMac;
        }

        /**
         * Sets the value of the clientMac property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setClientMac(String value) {
            this.clientMac = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Ip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "user", "password", "ip" })
    public static class Login {

        @XmlElement(name = "User", required = true)
        protected String user;
        @XmlElement(name = "Password", required = true)
        protected String password;
        @XmlElement(name = "Ip", required = true)
        protected String ip;

        /**
         * Gets the value of the user property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getUser() {
            return user;
        }

        /**
         * Sets the value of the user property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setUser(String value) {
            this.user = value;
        }

        /**
         * Gets the value of the password property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getPassword() {
            return password;
        }

        /**
         * Sets the value of the password property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Gets the value of the ip property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getIp() {
            return ip;
        }

        /**
         * Sets the value of the ip property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setIp(String value) {
            this.ip = value;
        }

    }

}
