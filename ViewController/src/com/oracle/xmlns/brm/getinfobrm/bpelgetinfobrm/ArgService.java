
package com.oracle.xmlns.brm.getinfobrm.bpelgetinfobrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ARGS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FLAGS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="POID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RESULTS"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LOGIN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_PACKAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AAC_PROMO_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TEMPLATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "args", "flags", "poid", "results", "template" })
@XmlRootElement(name = "ArgService")
public class ArgService {

    @XmlElement(name = "ARGS", required = true)
    protected ArgService.ARGS args;
    @XmlElement(name = "FLAGS", required = true)
    protected String flags;
    @XmlElement(name = "POID", required = true)
    protected String poid;
    @XmlElement(name = "RESULTS", required = true)
    protected ArgService.RESULTS results;
    @XmlElement(name = "TEMPLATE", required = true)
    protected String template;

    /**
     * Gets the value of the args property.
     *
     * @return
     *     possible object is
     *     {@link ArgService.ARGS }
     *
     */
    public ArgService.ARGS getARGS() {
        return args;
    }

    /**
     * Sets the value of the args property.
     *
     * @param value
     *     allowed object is
     *     {@link ArgService.ARGS }
     *
     */
    public void setARGS(ArgService.ARGS value) {
        this.args = value;
    }

    /**
     * Gets the value of the flags property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFLAGS() {
        return flags;
    }

    /**
     * Sets the value of the flags property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFLAGS(String value) {
        this.flags = value;
    }

    /**
     * Gets the value of the poid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPOID() {
        return poid;
    }

    /**
     * Sets the value of the poid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPOID(String value) {
        this.poid = value;
    }

    /**
     * Gets the value of the results property.
     *
     * @return
     *     possible object is
     *     {@link ArgService.RESULTS }
     *
     */
    public ArgService.RESULTS getRESULTS() {
        return results;
    }

    /**
     * Sets the value of the results property.
     *
     * @param value
     *     allowed object is
     *     {@link ArgService.RESULTS }
     *
     */
    public void setRESULTS(ArgService.RESULTS value) {
        this.results = value;
    }

    /**
     * Gets the value of the template property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTEMPLATE() {
        return template;
    }

    /**
     * Sets the value of the template property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTEMPLATE(String value) {
        this.template = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ACCOUNT_OBJ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "accountobj" })
    public static class ARGS {

        @XmlElement(name = "ACCOUNT_OBJ", required = true)
        protected String accountobj;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the accountobj property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getACCOUNTOBJ() {
            return accountobj;
        }

        /**
         * Sets the value of the accountobj property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setACCOUNTOBJ(String value) {
            this.accountobj = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LOGIN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_PACKAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AAC_PROMO_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="elem" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "login", "aacpackage", "aacpromocode" })
    public static class RESULTS {

        @XmlElement(name = "LOGIN", required = true)
        protected String login;
        @XmlElement(name = "AAC_PACKAGE", required = true)
        protected String aacpackage;
        @XmlElement(name = "AAC_PROMO_CODE", required = true)
        protected String aacpromocode;
        @XmlAttribute(name = "elem")
        protected Integer elem;

        /**
         * Gets the value of the login property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getLOGIN() {
            return login;
        }

        /**
         * Sets the value of the login property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLOGIN(String value) {
            this.login = value;
        }

        /**
         * Gets the value of the aacpackage property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACPACKAGE() {
            return aacpackage;
        }

        /**
         * Sets the value of the aacpackage property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACPACKAGE(String value) {
            this.aacpackage = value;
        }

        /**
         * Gets the value of the aacpromocode property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAACPROMOCODE() {
            return aacpromocode;
        }

        /**
         * Sets the value of the aacpromocode property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAACPROMOCODE(String value) {
            this.aacpromocode = value;
        }

        /**
         * Gets the value of the elem property.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getElem() {
            return elem;
        }

        /**
         * Sets the value of the elem property.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setElem(Integer value) {
            this.elem = value;
        }

    }

}
