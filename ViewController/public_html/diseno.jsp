<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
    <head>
        <title>TOTALPLAYZONE</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap/bootstrap.min.css" />
        <script src="<%=request.getContextPath()%>/assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/assets/js/popper.min.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/bootstrap/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/estilo.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/Montserrat.css" media="screen" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
        
        <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1132899846499742441']);
            cX.callQueue.push(['sendPageViewEvent']);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->
        </head>
    <body style="background: url(assets/img/background.png)no-repeat center center fixed; ">
    
        <div class="container">
            <div class="row" style="margin: 2em;">
                <div class="col-md-4 offset-md-4">
                    <img src="<%=request.getContextPath()%>/assets/img/LOGOTOTALPLAY.png" width="100%">
                </div>
            </div>
        </div>
    
         <div class="container" style="padding:0px; overflow: hidden">
        <div class="trans">
            <div class="row">
                <div class="col-md-4 offset-md-4" style="font-size: 25px; text-align: center" >REGISTRO</div>
            </div>
            
            <div class="row pt-2">
                <div class="col-md-4 offset-md-4">
                        <div class="inputDiv">
                            <input id="inputCta" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Escribe tu correo electr�nico" required class="col-sm-12">
                            <label class="helperUp col-sm-12" id="helperUsr"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Introduzca un correo electr�nico</label>
                            <label class="helperUp col-sm-12" id="helperDot"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Ejemplo de correo v�lido (micorreo@dominio.com)</label>
                        </div>
                </div>
            </div>
            
            <div class="row pt-3">
                <div class="col-md-4 offset-md-4 text-center">
                    <input type="checkbox" style="margin-right: 5px;" checked="checked" id="checkAviso" disabled>
                    <a href="#" id="linkModal">Acepto aviso de privacidad*</a>
                </div>   
            </div>
            
            <div class="row pt-3">
                <div class="col-3" ></div>
                <div class="col-6" >
                    <button type="button" class="boton w-100" id="conectar" >Enviar</button>
                </div>
                
                <div class="col-3" ></div>
            </div>
            
            <!--div class="row">
                <div class="col-md-4 offset-md-4" >
                    <button type="button" class="boton col-sm-10" id="conectar">Enviar</button>
                </div>
            </div-->
            
            <div class="row"></div>
            </div>
            
            <div class="row" style="margin-top: 20%">
                <div class="col-8"></div>
                <div class="col-2">
                    <img src="<%=request.getContextPath()%>/assets/img/conectate.png" class="logo3" >
                </div>
                 <div class="col-2"></div>
            </div>
            
     </div>
            
      
      <footer class="fixed-bottom" id="footern">
           <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-2">
                    <img src="<%=request.getContextPath()%>/assets/img/logoWGCvertical.png" class="logo1">
                </div>
                <div class="col-4">
                </div>
                <div class="col-2">
                    <img src="<%=request.getContextPath()%>/assets/img/logoGrupoSalinas.png" class="logo2">
                </div>
                 <div class="col-2"></div>
            </div>
            </div>
      </footer>
      
      
     
        <!--Modal hubo un error-->
        <div class="modal fade" id="errorLogin" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Oops!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-sm-5 offset-sm-5 circulo">
                                        <i class="fa fa-times fa-3x" aria-hidden="true" style="color:#e53935;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p id="txtErr1">Hubo un inconveniente al
                                    <br>conectarte a la red
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <p style="font-size:15px;" id="txtErr2">Te pedimos volver a intentar conectarte a la red.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="reintentar">Reintentar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal Conexi�n exitosa-->
        <div class="modal fade" id="exitoLogin" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Excelente!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-md-5 offset-md-5 circuloVerde">
                                        <i class="fa fa-check fa-3x" aria-hidden="true" style="color: #7bb342;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p>Te has conectado
                                    <br>exitosamente
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <p style="font-size:15px;">
                                       En un momento se te enviar� a la p�gina de Golf ....
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--div class="modal-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="salirRedirect">Salir</button>
                                </div>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
        <!--Modal de uso com�n-->
        <div class="modal fade" id="modalComun" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Oops!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-sm-5 offset-sm-5 circulo">
                                        <i class="fa fa-times fa-3x" aria-hidden="true" style="color:#e53935;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p >Lo sentimos
                                    <br>se acabaron los intentos
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--div class="modal-footer" id="footerComun">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="outoffattemps">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    
         <form  name="formAux" id="formAux" method="post"  action="" >
          <input  id="clave"  name="clave"  type="hidden"/> 
        </form>

         <iframe src="about:blank"  id="frameAux" name="frameAux" style="display:none"> </iframe>
       
    <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/settings.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/aes.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/pbkdf2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/AesUtil.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/peticion.js"></script>
    </body>
</html>