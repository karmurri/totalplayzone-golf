<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
    <head>
        <title>TOTALPLAYZONE</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap/bootstrap.min.css" >
        <script src="<%=request.getContextPath()%>/assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/assets/js/popper.min.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/bootstrap/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/estilo.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/Montserrat.css" media="screen" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
        
        <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1132904254190294463']);
            cX.callQueue.push(['sendPageViewEvent']);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131945376-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-131945376-2');
        </script>
        
    </head>
    <body>
        <nav class="navbar">
            <div class="col-md-3 offset-md-9 right divImg" style="margin-top: -8px;">
                <img src="<%=request.getContextPath()%>/assets/img/EncabezadoSuperiorDerecho_1.png" width="90%" class="float-right">
            </div>               
        </nav>
        <div class="container">
            <div class="row" style="margin-top:-5em;">
                <div class="col-md-4 offset-md-4">
                    <img src="<%=request.getContextPath()%>/assets/img/totalplay_logo_chico.png" width="100%">
                </div>
            </div>
        </div>
        <div class="container contenedorPrin" style="margin-top:30px;">
            <div class="row">
                <div class="col-md-4 offset-md-4 headerLogin">
                    �Bienvenido!
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-4 insideBody" id="insideBody">
                    <div class="container">
                        <div style="font-size:14px;!important" class="font-weight-bold">
                            <p><br>Ingresa los datos requeridos para poder<br>conectarte a la red de Totalplay WiFi.</p>
                        </div>
                        <div class="inputDiv">
                            <label class="col-sm-12">*Correo electr�nico</label>
                            <input id="inputCta" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Escribe tu correo electr�nico" required class="col-sm-12">
                            <label class="helperUp col-sm-12" id="helperUsr"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Introduzca un correo electr�nico</label>
                            <label class="helperUp col-sm-12" id="helperDot"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Ejemplo de correo v�lido (micorreo@dominio.com)</label>
                        </div>
                        <!--div class="inputDiv" id="inputDiv2">
                            <br>
                            <label class="col-sm-12">*Contrase�a</label>
                            <input id="inputPass" type="password" placeholder="Escribe tu contrase�a" required class="col-sm-12">
                            <label class="helperUp col-sm-12" id="helperPass"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Introduzca su contrase�a</label>
                        </div-->
                        <div id="divSalto">
                            <p><br><br></p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="container" id="btnAparte">
            <div class="row divBoton">
                <div class="col-md-4 offset-md-4" style="padding-left: 60px;">
                    <button type="button" class="boton col-sm-10" id="conectar">Enviar</button>
                </div>   
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 text-center">
                    <br>
                    <input type="checkbox" style="margin-right: 5px;" checked="checked" id="checkAviso" disabled>
                    <a href="#" id="linkModal">Acepto aviso de privacidad*</a>
                </div>   
            </div>
        </div>

        <footer class="fixed-bottom">
          <div class="container-fluid">
            <div class="row" style="background: linear-gradient(0deg, #3B4559 50%, white 50%);">
                <div class="col-md-3">
                    <img src="<%=request.getContextPath()%>/assets/img/FooterIzquierdo_2.png" width="70%">
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-3">
                    <img src="<%=request.getContextPath()%>/assets/img/FooterDerecho_tp.png" width="70%" class="float-right">
                </div>
            </div>
          </div>
        </footer>
        <!--Modal aviso de privacidad-->
        <div class="modal fade" id="avisoPrivacidad" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">Aviso de privacidad</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <p class="text-justify">
                            <strong>AVISO DE PRIVACIDAD � TOTAL PLAY</strong><br>
                            <strong>Identidad y Domicilio del Responsable</strong><br> 
                            
                            <br><strong>Total Play Telecomunicaciones, S.A. de C.V.,</strong> con domicilio convencional para o�r y recibir notificaciones en Insurgentes Sur 3579, Torre 3, PH, Colonia Tlalpan La Joya, Delegaci�n Tlalpan, C.P. 14000, Ciudad de M�xico, �nicamente para temas de privacidad y de protecci�n de datos personales (el <strong>�Responsable�</strong>), del tratamiento leg�timo, controlado e informado de los datos personales (los �Datos Personales�), de sus <strong>-Prospectos y clientes-</strong> (el <strong>�Titular�</strong>), y en congruencia con su pol�tica de privacidad, conforme a lo establecido en la Ley Federal de Protecci�n de Datos Personales en Posesi�n de los Particulares (la <strong>�LFPDPPP�</strong>) y dem�s normatividad secundaria vigente aplicable, as� como est�ndares nacionales e internacionales en materia de protecci�n de datos personales, con el prop�sito de garantizar la privacidad y el derecho a la autodeterminaci�n informativa y protecci�n de los Datos Personales, que el Responsable podr� recabar a trav�s de los siguientes medios: <strong>(i)</strong> de manera personal, cuando el Titular los proporciona de manera f�sica en nuestras instalaciones, <strong>(ii)</strong> de manera directa, cuando el Titular los ingresa a trav�s del sitio web www.totalplay.com.mx (el <storng>�Sitio Web�</strong>), <strong>(iii)</strong> de manera directa, cuando el Titular los proporciona v�a telef�nica, <strong>(iv)</strong> de manera indirecta, cuando otras empresas nos los transfieren, y <strong>(v)</strong> de manera indirecta, cuando se obtienen a trav�s de fuentes de acceso p�blico permitidas por la LFPDPPP; pone a disposici�n del Titular el presente aviso de privacidad integral (el <strong>"Aviso de Privacidad"</strong>) <strong>-previo a la obtenci�n de los Datos Personales-</strong> en estricto apego a los <strong>-principios de informaci�n, licitud, consentimiento, calidad, finalidad, lealtad, proporcionalidad y responsabilidad-</strong> contemplados en la LFPDPPP.<br>
                            
                            <br>Con motivo del servicio que ofrece el Responsable y en cumplimiento a lo establecido por el art�culo 3, fracciones LXIV y LXXI de la Ley Federal de Telecomunicaciones y Radiodifusi�n (la <strong>�LFT�</strong>), presta servicios de televisi�n, telefon�a e internet a usuarios finales, a trav�s de redes p�blicas de telecomunicaciones, mediante aceptaci�n del presente Aviso y el cumplimiento de los T�rminos y Condiciones del Servicio. <br>
                            
                            <br><strong>Datos Personales que ser�n sometidos a tratamiento</strong><br>
                            
                            <br><strong>Datos Personales No Sensibles</strong><br>
                            <strong>Identificaci�n:</strong> Nombre completo, domicilio completo, Clave �nica de Registro de Poblaci�n, Registro Federal de Contribuyentes con homoclave (RFC), identificaci�n oficial (tipo, n�mero y vigencia).<br>
                            <strong>Electr�nicos e Inform�ticos:</strong> N�mero de tel�fono fijo y celular, correo electr�nico, firma electr�nica fiable.<br>
                            <strong>Tr�nsito y Movimientos Migratorios:</strong> Pasaporte, Visa o FM3.<br>
                            
                            <br><strong>Datos Personales Financieros y/o Patrimoniales</strong>
                            <br><strong>Financieros:</strong> N�mero(s) de tarjeta(s) bancaria(s).<br>
                            
                            <br>De conformidad con lo establecido por el art�culo 16 de la LFPDPPP, se hace del conocimiento del Titular que el Responsable <strong>-no lleva a cabo el tratamiento de Datos Personales sensibles-.</strong><br> 
                            
                            <br><strong>Datos Personales de Menores e Incapaces</strong><br>
                            
                            <br>El Responsable no celebrar� operaciones que involucren el tratamiento de Datos Personales de menores de edad o de personas que se encuentren en estado de interdicci�n o incapacidad. <br>
                            
                            <br><strong>Finalidades del tratamiento de los Datos Personales</strong><br>
                            
                            <br>Con el prop�sito de brindarle los servicios de televisi�n, telefon�a e internet, el Responsable destinar� los Datos Personales para las finalidades que se indican a continuaci�n:<br>
                            
                            <br><strong><em>Finalidades Primarias.</em></strong> Necesarias para la existencia y cumplimiento de la obligaci�n jur�dica que el Titular establece con el Responsable, las cuales consisten en: <br>
                            <br><ol>
                                <li>Verificar y confirmar la identidad del Titular como cliente o prospecto para hacer uso de los servicios de televisi�n, telefon�a e internet.</li>
                                <li>Verificar que los Datos Personales contenidos en la Credencial para Votar que el Titular exhiba al Responsable para el otorgamiento de los servicios, coincidan con los que obran en poder del Instituto Nacional Electoral, en inter�s del Titular y evitar que el robo de identidad se materialice en alg�n fraude u otro delito en su perjuicio, as� como para la salvaguarda del inter�s p�blico.</li>
                                <li>Verificar la capacidad de pago de sus clientes y/o prospectos.</li>
                                <li>Otorgar los servicios de televisi�n, telefon�a e internet.</li>
                                <li>Contactarlo para concluir el proceso de formalizaci�n del Contrato de Servicios por el Responsable a trav�s del call center.</li>
                                <li>Cumplir con las obligaciones y ejercer los derechos derivados de la relaci�n jur�dica entablada entre el Responsable y el Titular.</li>
                                <li>La atenci�n de requerimientos de cualquier autoridad competente.</li>
                                <li>La realizaci�n de consultas, investigaciones y revisiones en relaci�n a cualquier queja o reclamaci�n por los servicios y/o productos del Responsable.</li>
                                <li>Realizar las gestiones correspondientes con el prop�sito de que los Datos Personales en todo momento se mantengan actualizados, correctos y completos, en cumplimiento al principio de calidad estatuido por la LFPDPPP y los Lineamientos.</li>
                            </ol>
                            <br><strong><em> Finalidades Secundarias.</em></strong> No son necesarias para la existencia y cumplimiento de la obligaci�n jur�dica derivada del Contrato de Servicios, que el Titular celebra con el Responsable, sin embargo, son complementarias e importantes, las cuales consisten en:
                            <br><ol>
                                <li>Elaborar perfiles de clientes y usuarios de los servicios de televisi�n, telefon�a e internet.</li>
                                <li>Enviar comunicados relacionados con ofertas, mensajes promocionales, comunicados con fines mercadot�cnicos, publicitarios y de prospecci�n comercial sobre servicios nuevos o existentes.</li>
                                <li>Aplicar encuestas, estudios de mercado, participar en eventos, concursos, trivias, juegos y sorteos, participar en redes sociales, chats e informaci�n que nos permita evaluar la calidad de los servicios.</li>
                            </ol><br>
                            <strong>Con qui�n se comparten los Datos Personales</strong><br>
                            <br>El Titular acepta y reconoce que el Responsable, <strong>�no requiere de su consentimiento para realizar transferencias nacionales o internacionales de los Datos Personales-</strong>, en t�rminos de lo estatuido por el art�culo 37 de la LFPDPPP, o en cualquier otro caso de excepci�n previsto por la legislaci�n aplicable, en el entendido de que el tratamiento que estos terceros otorguen a los Datos Personales deber� ajustarse a lo establecido en el Aviso de Privacidad, siempre y cuando se ubiquen en los siguientes supuestos: <br>
                            <br><ol>
                                <li>Cuando la transferencia est� prevista en una Ley o Tratado en los que M�xico sea parte.</li>
                                <li>Cuando la transferencia sea necesaria para la prevenci�n o el diagn�stico m�dico, la prestaci�n de asistencia sanitaria, tratamiento m�dico o gesti�n de servicios sanitarios.</li>
                                <li>Cuando la transferencia sea efectuada a <strong>�sociedades controladoras, subsidiarias o afiliadas bajo el control com�n del Responsable, o una sociedad matriz, o a cualquier sociedad del grupo empresarial al que pertenece el mismo, que opere bajo los mismos procesos y pol�ticas internas-.</strong></li>
                                <li>Cuando la transferencia <strong>-sea necesaria por virtud de un contrato celebrado o por celebrar en inter�s del Titular, por el Responsable y un tercero-.</strong></li> 
                                <li>Cuando la transferencia sea necesaria para la salvaguarda de un inter�s p�blico, o para la procuraci�n o administraci�n de justicia.</li>
                                <li>Cuando la transferencia sea precisa para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial, y</li>
                                <li>Cuando la transferencia sea precisa para el mantenimiento o cumplimiento de la relaci�n jur�dica derivada de los servicios y productos financieros formalizados con el Responsable.</li>
                            </ol><br>
                            <strong>Limitaci�n al uso o divulgaci�n de los Datos Personales</strong><br>
                            
                            <br>Es necesario hacerle saber al Titular que para restringir, limitar y controlar el tratamiento de los Datos Personales, el Responsable, cuenta con medidas administrativas, f�sicas y t�cnicas, adem�s de establecer pol�ticas y programas internos de privacidad para evitar la divulgaci�n de los Datos Personales, implementando diversos controles de seguridad. Los Datos Personales ser�n tratados de forma estrictamente confidencial, por lo que la obtenci�n, transferencia y ejercicio de los derechos derivados de los mismos, es mediante el uso adecuado, leg�timo y l�cito, salvaguardando de manera permanente los principios de licitud, consentimiento, informaci�n calidad, finalidad, lealtad, proporcionalidad y responsabilidad. <br>
                            
                            <br><strong>Registro P�blico para Evitar la Publicidad (�REPEP�) de la Procuradur�a Federal del Consumidor (�Profeco�)</strong><br>
                            
                            <br>Con el prop�sito de que el Titular pueda limitar el uso y divulgaci�n de los Datos Personales, adicionalmente podr� efectuar la inscripci�n correspondiente en el REPEP, el cual se encuentra a cargo de la Profeco, con la finalidad de que los Datos Personales no sean utilizados para recibir publicidad o promociones por parte de proveedores o empresas, en sus pr�cticas de mercadotecnia. Para mayor informaci�n de este registro, el Titular podr� consultar el portal de Internet de la Profeco http://www.gob.mx/profeco, o comunicarse al 01 800 468 8722.<br>
                            
                            <br><strong>Medio y Procedimiento para ejercer el Derecho de acceso, rectificaci�n, cancelaci�n y oposici�n (�Derechos ARCO�) y Revocaci�n del Consentimiento</strong><br>
                            <br>En virtud de que el Titular tiene derecho a la protecci�n de los Datos Personales, al acceso, rectificaci�n y cancelaci�n de los mismos, as� como a manifestar su oposici�n o revocaci�n, en los t�rminos que establece la LFPDPPP, podr� ejercer los Derechos ARCO o la revocaci�n del consentimiento en forma personal, o bien, a trav�s de su representante legal, mediante la generaci�n y env�o de la solicitud correspondiente al <strong>Departamento de Datos Personales</strong> del Responsable, para lo cual el Titular ejecutar� los siguientes:<br>

                            <br></strong>Pasos para Generar la Solicitud de Derechos ARCO (la �Solicitud�)</strong><br>
                            
                            <br><strong>Primero.-</strong> El Titular debe ingresar al sitio web www.datospersonalesgs.com.mx (el <strong>�Sitio Web DDP�</strong>);<br> 
                            <strong>Segundo.-</strong> Hecho lo anterior, har� click en la secci�n <strong><em>"Ejerce tus Derechos ARCO"</em></strong>, en la que aparecer� el recuadro (Reg�strate e Inicia Sesi�n), deber� llenar la informaci�n personal indicada para crear su <strong><em>usuario y contrase�a</em></strong> (el <strong>�Perfil�</strong>), y har� click en el bot�n <strong><em>�Ingresar�</strong></em>. Una vez creado el Perfil, el Titular podr� otorgar seguimiento puntual a la Solicitud.<br>
                            <strong>Tercero.-</strong> En seguida, el Titular har� click en el �cono <strong><em>"Nueva solicitud"</em></strong>, en el que aparecer� una pantalla (�Qu� empresas quieres consultar?), en la que ubicar� el logotipo del Responsable al que pretende formular la Solicitud;<br>
                            <strong>Cuarto.-</strong> Finalmente, el Titular elegir� el tipo de Derecho ARCO que decida ejercer, o bien, la revocaci�n del consentimiento, completando la informaci�n que se indica y har� click en el bot�n <strong><em>"Enviar Solicitud". </em></strong><br>
                            <br>El Departamento de Datos Personales recibir� la Solicitud y se pondr� en contacto con el Titular a trav�s del correo electr�nico establecido para tal efecto. Asimismo, el Titular podr� consultar el estatus de la Solicitud en cualquier momento, haciendo click en la secci�n <strong><em>"Ejerce tus Derechos ARCO"</em></strong> y dando click en el �cono <strong><em>"Seguimiento de Solicitudes".</em></strong><br>
                            <br>Para el caso de <strong><em>�solicitud de acceso-</em></strong>, proceder� previa acreditaci�n de la identidad del Titular o representante legal, seg�n corresponda, mediante la expedici�n de copias simples o documentos electr�nicos en poder del Responsable, y de manera gratuita, salvo los gastos de env�o o el costo de reproducci�n en copias u otros formatos. En el caso de que el Titular reitere la solicitud en un periodo menor a <strong>doce meses, deber� cubrir los costos correspondientes equivalentes a no m�s de tres d�as de Salario M�nimo Vigente en la Ciudad de M�xico en t�rminos de lo estatuido por la LFPDPPP.</strong><br>
                            
                            <br>Trat�ndose de <strong><em>�solicitud de rectificaci�n-</em></strong>, el Titular deber� indicar la modificaci�n a realizar, as� como aportar la documentaci�n que sustente la petici�n.<br>
                            
                            <br>Para efectos de las <strong><em>solicitudes de cancelaci�n de los Datos Personales</em></strong>, adem�s de lo dispuesto en el Aviso de Privacidad, se estar� a lo estatuido por el art�culo 26 de la LFPDPPP, incluyendo los casos de excepci�n de cancelaci�n de Datos Personales se�alados y los ordenados en el C�digo de Comercio y la LFT. <br>
                            
                            <br>Trat�ndose de la <strong><em>-solicitud de oposici�n-</em></strong> el Titular tendr� en todo momento el derecho a oponerse al tratamiento de los Datos Personales por causa leg�tima. De resultar procedente, el Responsable no podr� tratarlos.<br>
                            
                            <br>No proceder� el ejercicio del derecho de oposici�n en aquellos casos en los que el tratamiento sea necesario para el cumplimiento de una obligaci�n legal impuesta al Responsable.<br>
                            
                            <br>En t�rminos de lo estatuido por la LFPDPPP y el RLFPDPPP, el Responsable comunicar� al Titular, en un plazo m�ximo de <strong><em>20 d�as h�biles</em></strong> contados desde la fecha en que recibi� la solicitud, la determinaci�n adoptada, a efecto de que, si resulta procedente, se haga efectiva la misma dentro de los <strong><em>15 d�as h�biles</em></strong> siguientes a la fecha en la que se le comunica la respuesta.<br>
                            <br>El Responsable podr� negar el acceso a los Datos Personales o a realizar la rectificaci�n, la cancelaci�n o a conceder la oposici�n al tratamiento de los mismos, en los siguientes supuestos: <br>
                            <ul>
                                <li>Cuando el solicitante no sea el Titular o la representaci�n legal del mismo no est� debidamente acreditada. </li>
                                <li>Cuando en la base de datos del Responsable no se encuentren los Datos Personales. </li>
                                <li>Cuando se lesionen los derechos de un tercero. </li>
                                <li>Cuando exista un impedimento legal o la resoluci�n de una autoridad competente que restrinja el acceso a los Datos Personales o no permita la rectificaci�n, cancelaci�n u oposici�n de los mismos, y </li>
                                <li>Cuando el acceso, la rectificaci�n, la cancelaci�n o la oposici�n hayan sido realizadas.</li>
                            </ul>
                            <br>La negativa a que se refiere el p�rrafo que antecede podr� ser parcial, en cuyo caso el Responsable efectuar� el acceso, la rectificaci�n, la cancelaci�n o la oposici�n requerida por el Titular. <br>

                            <br>En los casos antes previstos, el Responsable informar� debidamente el motivo de la decisi�n al Titular o representante legal, en los plazos establecidos para tal efecto, por el mismo medio en que se llev� a cabo la solicitud o aquel se�alado por el Titular. En caso de que el Titular se sienta afectado con la resoluci�n que emita el Responsable, tiene disponible la acci�n de protecci�n de derechos ante el <strong>Instituto Nacional de Transparencia, Acceso a la Informaci�n y Protecci�n de Datos Personales</strong> (el <strong>�INAI�</strong>).<br> 
                            
                            <br><strong>Departamento de Datos Personales</strong><br>
                            
                            <br>Para cualquier <strong><em>�aclaraci�n, comentario, queja o inconformidad-</em></strong> con respecto a la pol�tica de privacidad del Responsable, el Titular podr� enviar su petici�n al <strong>Departamento de Datos Personales</strong> a trav�s del correo electr�nico datos personales <a href="totalplay@totalplay.com.mx" style="color:white;" target="_blank">totalplay@totalplay.com.mx</a>,  quien dar� respuesta a la petici�n en un plazo m�ximo de <strong><em>20 d�as h�biles.</em></strong> 
                            <br><br>Para el caso de que el Titular se vea impedido para generar y enviar la Solicitud de Derechos ARCO, as� como a manifestar la revocaci�n de su consentimiento en t�rminos de lo establecido en el Aviso de Privacidad, por motivo de fallas t�cnicas del Sitio Web DDP, podr� enviarla al Departamento de Datos Personales, a trav�s del correo electr�nico datospersonalestotalplay@totalplay.com.mx, previa acreditaci�n de la falla ocurrida, cumpliendo con los requisitos y con las condiciones consideradas por la LFPDPPP. <br>

                            <br><strong>Cookies y Protocolo de Seguridad en L�nea</strong><br>
                            
                            <br>El Responsable obtiene informaci�n, a trav�s del Sitio Web, mediante el uso de cookies, entendiendo como <strong><em>�Cookie�</em></strong> el archivo de datos que se almacena en el disco duro de la computadora del Titular, cuando este tiene acceso al Sitio Web. Dichos archivos pueden contener informaci�n tal como la identificaci�n proporcionada por el Titular, o informaci�n para rastrear las p�ginas de preferencia del mismo, para monitorear su comportamiento como usuario de internet, brindarles un mejor servicio y experiencia de usuario al navegar en el Sitio Web. Una Cookie no puede leer los datos o informaci�n del disco duro del Titular ni leer las cookies creadas por otros sitios o p�ginas. En caso de que el Titular desee rechazar las cookies o aceptarlas de manera selectiva, puede llevarlo a cabo a trav�s del ajuste de las preferencias del navegador. <br>
                            
                            <br><strong><em>Mecanismos remotos de comunicaci�n electr�nica que recaban los Datos Personales de manera autom�tica</em></strong><br>
                            
                            <br>Algunas partes del Sitio Web pueden utilizar <strong>Cookies y Web Beacons</strong> para simplificar la navegaci�n. Las <strong>Cookies</strong> son archivos de texto que son descargados autom�ticamente y almacenados en el disco duro del equipo de c�mputo del usuario al navegar en una p�gina de Internet espec�fica, que permiten recordar al servidor de Internet algunos datos sobre este usuario, entre ellos, sus preferencias de compra para la visualizaci�n de las p�ginas en ese servidor, nombre y contrase�a. Por su parte, las <strong>Web Beacons</strong> son im�genes insertadas en una p�gina de Internet o correo electr�nico, que puede ser utilizado para monitorear el comportamiento de un visitante, como almacenar informaci�n sobre la direcci�n IP del usuario, duraci�n del tiempo de interacci�n en dicha p�gina y el tipo de navegador utilizado, entre otros. Le informamos que utilizamos <strong>Cookies y Web Beacons</strong> para obtener informaci�n personal de usted, como la siguiente: <strong>(i).</strong> El tipo dispositivo, de navegador y sistema operativo que utiliza; <strong>(ii).</strong> Las p�ginas de Internet que visita en forma previa y posterior a la entrada del Sitio Web; <strong>(iii).</strong> Los v�nculos que sigue y permanencia en el Sitio Web; <strong>(iv).</strong> Su direcci�n IP; <strong>(v).</strong> Lugar desde el cual visita el Sitio Web y estad�sticas de navegaci�n. Estas <strong>Cookies</strong> y otras tecnolog�as pueden ser deshabilitadas. Puede buscar informaci�n sobre los navegadores conocidos y averiguar c�mo ajustar las preferencias de las <strong>Cookies</strong> en los siguientes sitios web: <br>
                            <br><strong>Microsoft Internet Explorer : </strong><a href="http://www.microsoft.com/info/cookies.htm" style="color:white;" target="_blank">http://www.microsoft.com/info/cookies.htm </a><br>
                            <br><strong>Mozilla Firefox : </strong><a href="http://www.mozilla.org/projects/security/pki/psm/help_21/using_priv_help.html" style="color:white;" target="_blank">http://www.mozilla.org/projects/security/pki/psm/<br>help_21/using_priv_help.html </a><br>
                            <br><strong>Google Chrome : </strong><a href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=es" style="color:white;" target="_blank">https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=es </a><br>
                            <br><strong>Apple Safari : </strong><a href="https://support.apple.com/es-es/guide/safari/sfri11471/mac" style="color:white;" target="_blank"> https://support.apple.com/es-es/guide/safari/sfri11471/mac </a><br>
                            <br>En el caso de empleo de <strong>Cookies</strong>, el bot�n de "ayuda" que se encuentra en la barra de herramientas de la mayor�a de los navegadores, le dir� c�mo evitar aceptar nuevas <strong>Cookies</strong>, c�mo hacer que el navegador le notifique cuando recibe una nueva cookie o c�mo deshabilitar todas las <strong>Cookies.</strong><br> 

                            <br><strong>Conservaci�n y Seguridad de los Datos Personales </strong><br>
                            
                            <br>El Responsable y/o sus encargados, conservar�n los Datos Personales durante el tiempo que sea necesario para procesar las solicitudes de productos y/o servicios ofrecidos, cumplir con la relaci�n jur�dica celebrada entre el Titular y el Responsable, as� como para mantener los registros contables, financieros y de auditor�a en t�rminos de la LFPDPPP y de la legislaci�n mercantil, fiscal y administrativa vigente. <br>
                            
                            <br>Los Datos Personales tratados por el Responsable y/o sus encargados se encontrar�n protegidos por medidas de seguridad administrativas, t�cnicas y f�sicas adecuadas contra el da�o, p�rdida, alteraci�n, destrucci�n o uso, acceso o tratamiento no autorizados, de conformidad con lo dispuesto en la LFPDPPPP, de la regulaci�n administrativa derivada de la misma, de la legislaci�n bancaria y normas secundarias emitidas por las autoridades competentes. <br>
                            
                            <br><strong>INAI </strong><br>
                            
                            <br>En caso de que el Titular considere que su derecho de protecci�n de los Datos Personales ha sido lesionado por el tratamiento indebido de los mismos, podr� interponer queja o denuncia correspondiente ante el INAI, por lo que podr� consultar el sitio web www.inai.org.mx para mayor informaci�n. <br>
                            
                            <br><strong>Exclusi�n de Responsabilidad </strong><br>
                            
                            <br>El Sitio Web, as� como el Sitio Web DDP, podr�an contener enlaces, hiperv�nculos o hipertextos <strong>�links�</strong>, banners, botones y/o herramientas de b�squeda en internet que al ser utilizados por los Titulares transportan a otros portales o sitios de internet que podr�an ser propiedad de terceros. Por lo tanto, no controla dichos sitios y no es responsable por los avisos de privacidad que ostenten, o en su caso, a la falta de ellos; los Datos Personales que los Titulares llegasen a proporcionar a trav�s dichos portales o sitios de internet distintos al Sitio Web, as� como al Sitio Web DDP, son su responsabilidad, por lo que deber� verificar el aviso de privacidad en cada sitio al que acceda. <br>
                            
                            Algunos v�nculos, banners y/o botones que solicitan Datos Personales dentro del Sitio Web son responsabilidad de terceros ajenos al Responsable, quienes en ocasiones son proveedores de servicios, por lo que se rigen por sus propios t�rminos y pol�ticas de privacidad. Para conocer m�s acerca de este tema, lo invitamos a consultar la secci�n t�rminos y condiciones. <br>
                            
                            <br><strong>Modificaciones al Aviso de Privacidad </strong><br>
                            
                            <br>El Responsable se reserva el derecho de actualizar o modificar peri�dicamente el Aviso de Privacidad conforme a los cambios de las pr�cticas de informaci�n, en atenci�n a las novedades legislativas, pol�ticas internas o nuevos requerimientos para la prestaci�n de los servicios. <br>
                            
                            Dichas actualizaciones o modificaciones, ser�n notificadas al Titular a trav�s del Sitio Web, en la secci�n Aviso de Privacidad. Se recomienda y requiere al Titular consultar el Aviso de Privacidad, por lo menos semestralmente, para estar actualizado de las condiciones y t�rminos del mismo. <br>
                            
                            <br><strong>Consentimiento para el tratamiento de los Datos Personales </strong><br>
                            <br>El Titular manifiesta que ha le�do, entendido y aceptado los t�rminos expuestos en el Aviso de Privacidad, lo que constituye el <strong>�consentimiento, libre, espec�fico, inequ�voco e informado-</strong>, inclusive con respecto a los cambios establecidos en las actualizaciones realizadas al mismo, con respecto al tratamiento de los Datos Personales en cumplimiento a lo establecido por la LFPDPPP y los Lineamientos. <br>
                            <br>
                            <br><strong>T�RMINOS Y CONDICIONES DEL SERVICIO- TOTALPLAY ZONE (�WiFi P�blico)</strong><br>
                            <br><strong>Cobertura�</strong><br>
                            <br>Toda contrataci�n o uso del servicio WIFI Totalplay Zone deber� estar dentro de la zona de cobertura que puede ser consultada en el Portal de Totalplay. Cualquier solicitud que se encuentre fuera de la zona de cobertura no podr� ser procesada. En caso de que existan restricciones el cliente ser� notificado oportunamente.<br>
                            <br><strong>Requisitos de contrataci�n</strong>
                            <ul>
                                <li>El cliente Total Play deber� registrarse a trav�s del Portal y/o Aplicaci�n Totalplay Zone indicando su n�mero de cuenta o DN (l�nea telef�nica) y su password.</li>
                                <li>Los usuarios que deseen conectarse al WiFi Totalplay Zone deber�n realizar el registro o login a trav�s de sus redes sociales y/o correo electr�nico.</li>
                                <li>Los clientes Totalplay y el p�blico en general deber�n leer y aceptar el Aviso de Privacidad as� como los T�rminos y Condiciones del Servicio.</li>
                            </ul><br>
                            <br><strong>Pol�tica de uso justo para Internet</strong><br>
                            <br>Al utilizar los servicios de transmisi�n de datos WiFi Totalplay Zone, usted acepta y est� de acuerdo en cumplir los t�rminos de esta pol�tica. El Servicio de Totalplay Zone, es comercializado para el uso estrictamente personal de Internet y no puede ser utilizado con prop�sitos comerciales. Queda prohibido el cobro a terceros por el uso del servicio que usted est� contratando. As� mismo, acepta que no podr� operar o permitir que terceros operen servidores de cualquier tipo y/o conectar dispositivos, hardware o software que brinden la funcionalidad de servidor a trav�s del servicio de Totalplay Zone. En todo momento usted ser� responsable del buen uso de la red y se compromete a utilizar el servicio de red de acuerdo con los t�rminos y condiciones establecidos. Usted se compromete a que las actividades que realice en el uso del servicio de Internet no limitar�n, bloquear�n o de cualquier forma afectar�n la red. En caso de que Totalplay detecte que el servicio no se est� usando  de acuerdo a cualquiera de las pol�ticas antes descritas, Totalplay Zone podr� cancelarle y/o suspenderle el servicio. Queda estrictamente prohibido utilizar el servicio de internet de Totalplay Zone, de forma que incumpla o viole las leyes, regulaciones, c�digos aplicables, as� como usos y buenas costumbres.<br>
                            <br>WiFi Totalplay Zone<br>
                            <br>Totalplay Zone es un servicio que ofrece �cobertura p�blica� WiFi para nuestros clientes, usuarios eventuales, de operadores fijos y m�viles, utilizando nuestra propia infraestructura o de cualquier red de acceso WiFi de terceros. Totalplay Zone a trav�s del registro en la aplicaci�n o en el portal, provee servicio de WiFi P�blico con el que podr�s disfrutar de internet a alta velocidad garantizando la mejor experiencia de conectividad por medio de tus dispositivos m�viles, tablet, smartphone, computadora personal o laptop.<br>
                            <br>La disponibilidad de los canales del servicio y contenidos se modificar� peri�dicamente a discreci�n de Totalplay Zone o a solicitud de sus proveedores de contenido. Al hacer uso de este servicio, el cliente acepta incondicionalmente los T�rminos y Condiciones de uso que se definen a continuaci�n:<br>
                            El servicio WiFi de Totalplay Zone estar� disponible para clientes que tengan contratado cualquier servicio con Total Play. Para poder acceder a la plataforma, deber�s de contar con un usuario y contrase�a, el cual puede ser obtenido de manera gratuita registr�ndose en el portal de Mi Totalplay (<a href="https://www.mitotalplay.com.mx" style="color:white;" target="_blank">https://www.mitotalplay.com.mx</a>).<br>
                            <br>Para el registro, se te solicitar� tu n�mero de cuenta de Totalplay o tu n�mero de tel�fono o tu direcci�n de email que registraste en tu contrato. Estos datos los puedes conocer revisando tu �ltimo estado de cuenta o llamando al n�mero de Atenci�n a Clientes 1579 8000 � 01800 510 0510 las 24hrs.<br>
                            <br>El servicio de Totalplay Zone y todos los contenidos disponibles en la plataforma son para uso personal y no comercial. No se podr� descargar, modificar, copiar, distribuir, transmitir, mostrar, ejecutar, reproducir, duplicar, publicar, otorgar licencias, crear obras derivadas del uso del servicio u ofrecer en venta la informaci�n obtenida del servicio. Totalplay Zone no promueve, ni alienta ni tolera la copia de pel�culas, series, contenido provisto en forma digital o ninguna otra actividad il�cita. Est� estrictamente prohibido evitar, eliminar, modificar, desactivar, interferir o burlar las protecciones de contenido del servicio de Totalplay Zone.<br>
                            <br>Est� prohibido enmascarar o utilizar t�cnicas de enmascaramiento (framing) para ocultar una marca, un logotipo u otra informaci�n protegida por leyes de Propiedad Intelectual (tales como im�genes, textos, diagramas de p�ginas o formas) de Totalplay.<br>
                            <br>Totalplay es ajeno al uso de los dispositivos electr�nicos, gastos adicionales de transmisi�n de datos o cobros de terceros por el acceso a Internet, quedando esto bajo responsabilidad del cliente.<br>
                            <br>Total Play Telecomunicaciones S.A. de C.V. pondr� a disponibilidad de sus clientes aplicaciones capaces de reproducir los contenidos y servicios ofrecidos por Totalplay Zone, este portal  o aplicaci�n puede incluir dispositivos m�viles, computadoras personales, tablets digitales y televisores conectados a Internet. Por lo que Totalplay no asume ninguna responsabilidad ni garantiza el rendimiento de estos dispositivos, ni la compatibilidad del servicio en ellos. Al hacer uso del servicio, el cliente acuerda recurrir �nicamente al fabricante o vendedor del dispositivo en caso de haber alg�n inconveniente con el mismo o con su compatibilidad con el servicio de WiFi Totalplay Zone.<br>
                            <br>Las aplicaciones para dispositivos m�viles se podr�n obtener desde las siguientes tiendas en l�nea:<br>
                            <ul>
                                <li>App Store</li>
                                <li>Google Play</li>
                            </ul>
                            <br>Los requisitos m�nimos para utilizar el servicio desde computadoras personales o laptops son:<br>
                            <ul>
                                <li>Windows 95 SP2 con las versiones m�s recientes de Internet Explorer, FireFox y Chrome.</li>
                                <li>Mac OS X con las versiones m�s recientes de Safari y Firefox</li>
                            </ul><br>
                            Cuando se accede al servicio se te solicitar� instalar un plug-in, el cual es necesario para la reproducci�n de los contenidos, por lo que deber�s de aceptar su instalaci�n. <br>
                            <br>Podr�s instalar el servicio de Totalplay Zone en cinco dispositivos como m�ximo y estos pueden ser utilizados de forma simult�nea. Si deseas agregar un 6� dispositivo, deber�s de dar de baja uno de los  cinco dispositivos que previamente hab�as configurado.<br>
                            <br>El Cliente acepta recibir contenidos publicitarios (banners, v�deos, encuestas, juegos, etc.) bajo la conexi�n de internet WiFi Totalplay Zone.<br>
                            <br>El Cliente acepta el env�o de notificaciones cuando se encuentre en una zona WiFi Totalplay Zone. Al usar el WiFi de Totalplay Zone se evitan los cobros adicionales por el uso excesivo de los datos m�viles en sus dispositivos. No solo disfrutar� de una conexi�n m�s r�pida que la de los datos m�viles, sino que tambi�n ahorrar� dinero mientras accede a sus aplicaciones, p�ginas web y redes sociales.<br>
                            <br>El Cliente acepta que el uso que va a hacer de estos servicios ser� conforme a todas las leyes que le sean aplicables.<br>
                            <br><strong>Actualizaciones</strong>: Al aceptar estos T�rminos y Condiciones el Cliente reconoce y acepta que su sistema ser� utilizado para recibir y servir actualizaciones a trav�s de un protocolo peer to peer.<br>
                            <br><strong>Consentimiento para comunicaciones electr�nicas</strong>: Totalplay Zone puede enviar al Cliente avisos legales y otras comunicaciones sobre el servicio o hacer uso de la informaci�n que proporciona el Cliente. Totalplay Zone enviar� las comunicaciones de avisos a trav�s del servicio o por correo electr�nico a la direcci�n del usuario principal de correo electr�nico registrada o publicar� las Comunicaciones en sus Sitios Web. Al aceptar los T�rminos y Condiciones, el Cliente da su consentimiento para recibir todas las comunicaciones a trav�s de estos medios electr�nicos.<br>
                            <br><strong>Tecnolog�a de recolecci�n de datos</strong>: Totalplay Zone le informa de que en algunos programas o productos puede utilizar tecnolog�a de recolecci�n de datos para recopilar la informaci�n t�cnica (incluidos los archivos sospechosos), para mejorar los servicios, para ofrecer servicios relacionados, para su adaptaci�n y para evitar la utilizaci�n ilegal o sin licencia del servicio o los da�os resultantes de los productos de malware.<br>
                            <br>La informaci�n personal facilitada (nombre, direcci�n de e-mail, contrase�a), durante la configuraci�n inicial, se recopila y se utilizar� como un nombre de cuenta bajo el cual podr� elegir recibir servicios adicionales y/o bajo el cual podr� utilizar ciertas caracter�sticas del portal o de la aplicaci�n<br>
                            <br>El Cliente acepta que Totalplay Zone utilizar� esa informaci�n como parte de los servicios prestados en relaci�n con el producto y para prevenir y detener el funcionamiento de programas de malware en su equipo.<br>
                            <br>De tiempo en tiempo, Totalplay Zone puede recopilar determinada informaci�n del equipo que se est� conectando, que puede incluir: Tipo de dispositivo, Direcci�n IP, informaci�n sobre riesgos de seguridad potenciales adem�s de URLs de sitios web visitados que pueden ser potencialmente peligrosos. Las URLs podr�an contener informaci�n personalmente identificable que un sitio web potencialmente fraudulento est� intentando conseguir sin su permiso. Esta informaci�n es recopilada por Totalplay Zone con el fin de evaluar y mejorar la capacidad para detectar comportamientos maliciosos, sitios web potencialmente fraudulentos y otros riesgos de seguridad de Internet. Esta informaci�n no ser� cotejada con ninguna informaci�n personal identificable del Cliente.<br>
                            <br>Las URL�s de los sitios Web visitados adem�s de las palabras clave de b�squeda y los resultados de b�squeda s�lo si est� activada la caracter�stica de barra de herramientas del navegador, ser� recopilada por Totalplay Zone para su an�lisis y una vez concluida la vigencia del licenciamiento la informaci�n ser� eliminada. FIN DEL DOCUMENTO
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="aceptaAviso">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal hubo un error-->
        <div class="modal fade" id="errorLogin" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Oops!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-sm-5 offset-sm-5 circulo">
                                        <i class="fa fa-times fa-3x" aria-hidden="true" style="color:#e53935;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p id="txtErr1">Hubo un inconveniente al
                                    <br>conectarte a la red
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <p style="font-size:15px;" id="txtErr2">Te pedimos volver a intentar conectarte a la red.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="reintentar">Reintentar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal Conexi�n exitosa-->
        <div class="modal fade" id="exitoLogin" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Excelente!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-md-5 offset-md-5 circuloVerde">
                                        <i class="fa fa-check fa-3x" aria-hidden="true" style="color: #7bb342;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p>Te has conectado
                                    <br>exitosamente
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <p style="font-size:15px;">
                                    Bienvenido a la experiencia TotalplayZone, disfruta de
                                    <br>la m�xima velocidad que te ofrece Totalplay.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="salirRedirect">Salir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal de uso com�n-->
        <div class="modal fade" id="modalComun" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalHeader" style="background-color:#8154A1;">
                        <div style="margin-top:5px;">
                            <h3 class="text-center">�Oops!</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 offset-md-12">
                                    <div class="col-sm-5 offset-sm-5 circulo">
                                        <i class="fa fa-times fa-3x" aria-hidden="true" style="color:#e53935;"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-weight: bold; font-size: 25px;">
                                <div class="col-md-12 offset-md-12" style="text-align: center;">
                                    <p >Lo sentimos
                                    <br>se acabaron los intentos
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="footerComun">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center">
                                    <button type="button" class="boton col-sm-12" id="outoffattemps">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/settings.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/aes.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/pbkdf2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/AesUtil.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/peticion.js"></script>
        
        <iframe src="about:blank"  id="frameAux" name="frameAux" style="display:none"> </iframe>
    </body>
</html>