<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<html>
    <head>
        <title>TOTALPLAYZONE-INDEX</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap/bootstrap.min.css" >
        <script src="<%=request.getContextPath()%>/assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/assets/js/popper.min.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/bootstrap/bootstrap.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/estilo.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/Montserrat.css" media="screen" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
        
        <!-- Cxense script begin -->
        <script type="text/javascript">
            var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['setSiteId', '1132904254190294463']);
            cX.callQueue.push(['sendPageViewEvent']);
            </script>
            <script type="text/javascript">
            (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
            e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
            t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
        </script>        
        <!-- Cxense script end -->

    </head>
    <body></body>
    <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/settings.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/aes.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/pbkdf2.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/AesUtil.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script>
</html>